#!/usr/bin/env python

import socketio
import eventlet
import eventlet.wsgi
import os, sys, inspect
import cv2
from flask import Flask, render_template

### Load SynOSCopy library
cmd_subfolder = os.path.realpath(
        os.path.abspath(
            os.path.join(
                os.path.split(
                    inspect.getfile(
                        inspect.currentframe()))[0],
                    "..",
                    "..",
                    "py-player", 
                    "SynOSCopy")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)
from Bundle import *


## OpenCV specific
(cvmajor, _, _) = cv2.__version__.split(".")[:3]
if (int(cvmajor) >= 3):
    color_flag = cv2.IMREAD_COLOR
    conversion_flag = cv2.COLOR_BGR2HSV
else:
    color_flag = cv2.CV_LOAD_IMAGE_COLOR
    conversion_flag = cv2.CV_BGR2HSV

## web app and sockets
sio = socketio.Server()
app = Flask(__name__)


class myimg:

    def __init__(self, filename):
        self.filename = filename
        self.offsetx = 0
        self.offsety = 0
        self.offsetw = 0
        self.offseth = 0
        self.xfactor = 1
        self.yfactor = 1
        self.zoom = 160
        self.posx = 0
        self.posy = 0
        self.img = cv2.imread("static/{}".format(self.filename), color_flag)
        (self.height, self.width, _) = self.img.shape

    def set_size(self, data):
        self.offsetx = data['offsetLeft']
        self.offsety = data['offsetTop']
        self.offsetw = data['offsetWidth']
        self.offseth = data['offsetHeight']
        self.xfactor = self.height / self.offseth
        self.yfactor = self.width / self.offsetw

    def get_hues(self):
        x1, y1, x2, y2 = self.get_window()
        pixelhsv = cv2.cvtColor(self.img[y1:y2, x1:x2 ], conversion_flag)
        tmp = cv2.calcHist([pixelhsv], [0], None, [12], [0, 180]).astype(float)
        tmp = tmp / tmp.sum()
        hue_perc = [x for bb in tmp for x in bb]
        return hue_perc

    def get_window(self):
        x, y = self.get_real_pos()
        xd = self.zoom/2 * self.xfactor
        yd = self.zoom/2 * self.yfactor
        return [max(x-xd, 0), max(y-yd, 0),
                min(x+xd, self.width), min(y+yd, self.height)]

    def get_real_pos(self):
        x = (self.posx - self.offsetx) * self.xfactor
        y = (self.posy - self.offsety) * self.yfactor
        return(x,y)


ti = myimg('mondrain03.jpg')
#ti = myimg('circular.jpg')
#ti = myimg('colorwheel.png')
# ti = myimg('hue.png')


## get html
@app.route('/')
def index():
    """Serve the client-side application."""
    return render_template('index.html', imgfile=ti.filename)

## move
@sio.on('mouse move', namespace='/movement')
def mouse_move(sid, data):
    ti.posx = data['x']
    ti.posy = data['y']
    hsv = ti.get_hues()
    bndl = Bundle('127.0.0.1', 8000)
    mxlen = len(hsv)#//3
    for i in range(0, mxlen):
        bndl.add_voice_on(osc_note=float(hsv[i]), voice_id=i+1)#,
                #velocity=hsv[mxlen+i])
        #bndl.add_volume(voice_id=i+1, volume=hsv[2*mxlen+i])
    bndl.send()
    #(x,y) = ti.get_real_pos()
    #print("X:{}, Y:{}".format(x,y))
    #print(ti.get_hues())

## zoom in/out
@sio.on('zoom', namespace='/movement')
def mouse_zoom(sid, data):
    ti.zoom = data
    print(data)

## Window resize
@sio.on('resize', namespace='/movement')
def window_resize(sid, data):
    print("x-offset:{}, y-offset:{}".format(data['offsetLeft'],
        data['offsetTop']))
    ti.set_size(data)


if __name__ == '__main__':
    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(sio, app)
    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 9000)), app)
