#!/usr/bin/env python

"""
Player for "The Exciting Synesthesia Machine". Receives stream from
synesthesia-capture and sends it to OSC.

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import argparse, logging
import os, sys, inspect
import socket

### Load SynOSCopy library
cmd_subfolder = os.path.realpath(
        os.path.abspath(
            os.path.join(
                os.path.split(
                    inspect.getfile(
                        inspect.currentframe()))[0],
                    "..", 
                    "py-player", 
                    "SynOSCopy")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)
from Bundle import *
### End loading

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)

def read_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--readport"
            , type=int
            , default=52375
            , help="The PORT where the receiving machine listens")
    parser.add_argument(
        "--targetip"
        , default="127.0.0.1"
        , help="The ip of the OSC receiver")
    parser.add_argument(
        "--targetport"
        , type=int
        , default=8000
        , help="The port the OSC server is listening on")
    return parser.parse_args()


if __name__ == "__main__":
    arguments = read_arguments()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("", arguments.readport))
    logging.info("Reading from %s" % str(sock.getsockname()))
    logging.info("Sending OSC to %s:%d" % (arguments.targetip,
        arguments.targetport))
    while True:
        data, addr = sock.recvfrom(512) 
        hsv = data.decode('utf-8').split(',')
        bndl = Bundle(arguments.targetip, arguments.targetport)
        mxlen = len(hsv)//3
        for i in range(0, mxlen):
            bndl.add_voice_on(osc_note=float(hsv[i]), voice_id=i+1,
                    velocity=hsv[mxlen+i])
            bndl.add_volume(voice_id=i+1, volume=hsv[2*mxlen+i])
        bndl.send()
