#!/usr/bin/env python

"""
camera capture for "The Exciting Synesthesia Machine". Streams computed
histograms to synesthesia-player on targetip and targetport.

author: David Bridges <David.Bridges@Plymouth.ac.uk>
	Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import argparse, logging
import numpy as np
import cv2
import datetime
import socket

logging.basicConfig(
    level=logging.WARNING,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)


(cvmajor, _, _) = cv2.__version__.split(".")[:3]
conversion_flag = cv2.COLOR_BGR2HSV
color_flag = cv2.IMREAD_COLOR
if (int(cvmajor) < 3):
    color_flag = cv2.CV_LOAD_IMAGE_COLOR

def read_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--targetip"
            , default="192.168.0.5"
            , help="The IP address of the receiving machine")
    parser.add_argument(
            "--targetport"
            , type=int
            , default=52375
            , help="The PORT where the receiving machine listens")
    parser.add_argument(
            "--cameraid"
            , type=int
            , default=0
            , help="The ID of the camrera (usually '0')")
    parser.add_argument(
            "--colors"
            , type=int
            , default=12
            , help="Number of colors (default: 12)")
    parser.add_argument(
            "--showvideo"
            , dest="videoout"
            , action="store_true"
            , help="output video stream")
    parser.add_argument(
            "--centerx"
            , type=int
            , default=320
            , help="x coordinate of the focus point")
    parser.add_argument(
            "--centery"
            , type=int
            , default=240
            , help="x coordinate of the focus point")
    parser.add_argument(
            "--size"
            , type=int
            , default=160
            , help="size of the focus point")
    parser.set_defaults(videoout=False)
    return parser.parse_args()


def get_hsv_distribution(pixels, nbcolors):
    pixelhsv = cv2.cvtColor(pixels, conversion_flag)
    # import pdb; pdb.set_trace();
    tmp = cv2.calcHist([pixelhsv], [0], None, [nbcolors], [0, 180]).astype(float)
    tmp = tmp / tmp.sum()
    # hue_perc = [x for bb in tmp for x in bb]
    hue_perc = tmp[:,0]
    tmp = cv2.calcHist([pixelhsv], [1], None, [nbcolors], [0, 256]).astype(float)
    tmp = tmp / tmp.sum()
    saturation_perc = [x for bb in tmp for x in bb]
    saturation_perc = tmp[:,0]
    tmp = cv2.calcHist([pixelhsv], [2], None, [nbcolors], [0, 256]).astype(float)
    tmp = tmp / tmp.sum()
    # value_perc = [x for bb in tmp for x in bb]
    value_perc = tmp[:, 0]
    return [hue_perc, saturation_perc, value_perc]


if __name__ == "__main__":
    arguments = read_arguments()

    cap = cv2.VideoCapture(arguments.cameraid)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    lu_x = arguments.centerx - arguments.size/2
    lu_y = arguments.centery - arguments.size/2
    rl_x = arguments.centerx + arguments.size/2
    rl_y = arguments.centery + arguments.size/2

    while(True):
        #http://docs.opencv.org/trunk/d4/d73/tutorial_py_contours_begin.html
        #http://www.pyimagesearch.com/2014/12/15/real-time-barcode-detection-video-python-opencv/
        ret, frame = cap.read()
        # import pdb; pdb.set_trace();
        f2 = frame[lu_y:rl_y, lu_x:rl_x]
        [h_v, s_v, v_v] = get_hsv_distribution(f2, arguments.colors)

        if(arguments.videoout):
            # mask = frame.copy()
                cv2.rectangle(frame, (lu_x, lu_y), (rl_x, rl_y), (99, 66, 33), 1, 8)
                cv2.imshow("frame",frame)
                # cv2.imshow("f2",f2)

        # h_str = ",".join([str(x) for x in h_v])
        # s_str = ",".join([str(x) for x in s_v])
        # v_str = ",".join([str(x) for x in v_v])
        h_str = ",".join(h_v.astype(str))
        s_str = ",".join(s_v.astype(str))
        v_str = ",".join(v_v.astype(str))
	# print(s_str)
        logging.info("%s, %s" % (datetime.datetime.now().isoformat() , h_str))
        sock.sendto(bytearray("%s,%s,%s" %(h_str, s_str, v_str)), (arguments.targetip, arguments.targetport))
        #if cv2.waitKey(1) & 0xFF == ord('q'):
        #    break

    # When everything done, release the capture
    cap.release()
