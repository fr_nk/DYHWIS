# SynOSCopy protocol (modified version)

This protocol is used in the project ["Remapping the sensorium: Do You Hear What I See?"](https://collaboratoire.cognovo.eu/projects#p2), which again is part of the [CogNovo](https://CogNovo.eu) summer school ["ColLaboratoire"](https://ColLaboratoire.CogNovo.eu).

In our setup we had [puredata (pd)](https://puredata.info) on the receiving side of this protocol. Its missing int32 data type (integer is mapped on float32 and looses precision for numbers > 24bit) required a redefinition of the SynOSCopy protocol. The changes are documented below, only the parts relevant for this project are documented at the moment.

## Overview

On top of the address pattern is the `/SYN` part. `/SYN` itself has not got any arguments.

Under `/SYN` is the `/IDx` part located. You can think of it as the big brother of the MIDI channel. Every synth has got its own number, it starts with `/SYN/ID1`, then `/SYN/ID2` and so on. It is also used to access synth-sub-patches as with MIDI multimode. In this case, the different patches are accessed by `/SYN/ID1.1`, `/SYN/ID1.2` and so on. Remember: when you want to set arguments for several IDs at once you can use OSCs wildcard system to do so. E.g. `/SYN/ID*` sets arguments for all IDs and `/SYN/ID1.*` for all synth-sub-patches within ID1. The IDx-System is good for cases where the sending OSC client does not know who he is speaking with (see "OSC-Hello" in chapter "Some Words"). For other cases the differing IDs can also have an additional plain name which is in the same hierachy `/SYN/name/`. If one argues that the ID part is unnecessary as messages could be directly sent to the regarding synth, note that the recipient could be a host, hosting several synthesizers, or a synth with several channels, each a separate synthesizer (like with many MIDI-enabled synths which have the possibility of holding 16 different instruments).

Under `/IDx` is the `/Vx` part located. V stands for **voice**. In the SYN proposal, you do not turn on notes, you turn on voices which have got a note or frequency argument. The big advantage of this system is that you can e.g. have as many "c3" notes playing as you like, each one with different filter cutoff values set or whatever else you can think of.


## Datatypes

In this proposal, mostly OSC's float32 data type is used (and True, False and Nil for flags). The reason to switch to float instead of using int32 as in the original SynOSCopy protocol is the missing support for 32 bit integer in pure data. Many of the absolute values are within the range between 0.0 and 1.0, but note numbers and frequencies can also be sent without any translation and with a high precision. While absolute values only make sense in the positive range, relative numbers can also be negative.

If bigger data types are needed in future, the float32 datatype `f` can easily be exchanged by the float64 datatype `d`.

General arguments as ID's and voice's **parameters**, **pedals**, **volume**, **velocity**, **program change** and **marker numbers** simply go from minimum 0.0 to maximum 1.0. Volume is sent linear (linear fader position) but should of course be interpreted logarithmic by the recipient. (So the transmitted volume value is a bit like a percentage of loudness).

**Panning**'s middle position is 0.5. Completely to the left/back/bottom (X-Y-Z) is 0.0 and completely to the right/front/top is 1.0.

**Pitch** and **frequency** can either be passed in cent or in Hz without any conversion. A frequency of 328.5Hz can be sent exactly as that -- *328.5*. The same goes for Tempo, which is sent in BPM.

**Time** (and **Song Position**) can be sent in seconds or beats. Note the change compared to the original SynOSCopy protocol where time is sent in milliseconds. But using floats, a millisecond is 0.001 second after all.

**Notes** are coded in the following way: Middle C (= C4 = MIDI note number 60) equals the float32 value 60.0. Each note up/down is +1.0/-1.0.

## Methods

There are several Voice-Level and ID-Level Methods.

### Per-Voice Methods

Per voice the following methods can be invoked:

`/SYN/IDx/Vx/ON,TFTf(Ff)` Turns on a defined voice. Passed are either the note number or the frequency as fourth argument. It is also possible to set velocity with arguments five and six. If no velocity value is submitted, the recipient should either use a standard velocity value or use the one from the last received voice on message. The first argument tells the receiver to either cut a still sounding voice with the given voice number (`T`) or to let the old voice release and play in parallel with the new voice this message creates. This might not be implemented in many synthesizers, but if, it has a big impact on the receivers' voice management! In the second case, the receiver has to create a second sounding voice with a different internal voice number. The old voice then is no more tweakable from SynOSCopy. The problem why this was introduced is that a SynOSCopy sender does not know when a voice has finished its release phase after a Voice-Off message has been sent. So a following Voice-On with the same voice number could cut a voice in a releasing phase. This is not so bad if the same note number / frequency is played (like in MIDI). But with different frequencies, this easily gets noticed negatively. When the second argument is `T`, the fourth argument is relative to the frequency/note number of the last voice on message. The third argument tells if either a note number (`T`) or an absolute frequency (`F`) is passed with the fourth argument. It is important that both possibilities exist for two contrary kinds of synthesizers: 1) drum-machine like ones which trigger a predefined sample which is mapped to a specific key and 2) other synthesizers which play a specific frequency, also think of breath controllers here (or Theremin-like stuff). (One of the problems with integrating SynOSCopy with OMC was that OMC favoured the key-based approach) If the fifth parameter is `T`, the sixth argument (velocity) is relative to the last submitted velocity value. It has a big advantage when notes can be / usually are turned on by frequency and not note number: when multiple controllers control one synth, the synth does not have to keep track which controller's note number means which frequency (dynamic retuning plays a big role in this proposal).

`/SYN/IDx/Vx/OFF,(Ff)` Turns off a defined voice. It is also possible to submit velocity with this function (like you know from note off velocity in MIDI). If the first parameter is `T`, the second argument (velocity) is relative to the voice on velocity value of this voice.

`/SYN/IDx/Vx/VOL,Ff` Sets the volume of the defined voice. When the first argument is `T`, the second argument is relative.

`/SYN/IDx/Vx/PITCH,FFf` Pitches the specified voice. When the first argument is`T`, the third argument is relative. When the second argument is `F`, the third argument passes cents (then the first argument is ignored and set to relative as cents are always relative), when it is `T` it passes Hz.

`/SYN/IDx/Vx/PAN,Ff(ff)` Pans the specified voice. When the first argument is `T`, the following arguments are relative. If just arguments one and two are passed, it is a simple left-right-pan. When a third argument is passed too, the panning becomes an X-Y panning. And when a fourth argument is passed, panning is 3-dimensional X-Y-Z.

`/SYN/IDx/Vx/P1,Ff` Sets parameter 1 of the voice. This could e.g. be the note aftertouch. When the first argument is `T`, the second argument is relative. Parameter 2 is method `/SYN/IDx/Vx/P2`, then comes P3 etc.


# Changes

2016-08-07: import from [SynOSCopy wiki](https://github.com/fabb/SynOSCopy/wiki), documented the voice based methods used in the DYHWIS project 


# Problems / Questions?

If you have questions, please contact [Frank](https://CogNovo.eu/frank-loesche).
