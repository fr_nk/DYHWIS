from minisom import MiniSom
from numpy import genfromtxt,array,linalg,zeros,mean,std,apply_along_axis
import pickle, sys
import numpy as np
import matplotlib.pyplot as plt

def som_mod(filename):
    with open(filename,"rb") as f:
        data = pickle.load(f)
        data_dim = data.shape[1]
        print("Init SOM with ", data_dim)

        som = MiniSom(60,60,data_dim,sigma=1.,learning_rate=.5)
        som.random_weights_init(data)
        print("Training...")
        som.train_random(data,3000) # random training
        print("\n...ready!")

        return som, data

if __name__ == "__main__":
    #script name and filename
    if len(sys.argv)<2:
        sys.exit(-1)
        print("Specify input file name for the modality")

    filename= sys.argv[1]

    som,data = som_mod(filename)

    winners = np.zeros((60, 60))
    for d in data[::20]:

        winners[som.winner(d)] +=1

    import pdb; pdb.set_trace()

    for x,w in enumerate(winners):
        for y,ww in enumerate(w):
            plt.scatter(y,x,ww)
    plt.show()
