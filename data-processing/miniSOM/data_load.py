import pickle
import numpy as np
import csv
import sys

if __name__ == "__main__":
    #script name and filename
    if len(sys.argv)<2:
        sys.exit(-1)
        print("Specify input file name")

    filename= sys.argv[1]

    with open(filename, 'r') as csvfile:

        spamreader = csv.reader(csvfile, delimiter=',')

        pois = []
        data = []
        for row in spamreader:
            if pois == []:
                for ir,r in enumerate(row):
                    if "_val" in r:
                        pois.append(ir)
            else:

                d= [row[p] for p in pois]
                data.append(d)

        data = np.array(data,dtype=float)
        print(data.shape)

    f = open("eeg_record.pickled","wb")
    pickle.dump(data,f)
    f.close()
