# A module for holding normalisation functions for transforming data
# Author: David Bridges (david.bridges@plymouth.ac.uk)

import pandas as pd
import numpy as np
import normz # module with transformation functions
import math, glob, os, datetime, scipy.stats

def transforms (data):
		""" 
		Transform data between 0 and 1 
		"""
		# Winsorise values - trim to 95% limit
		
		data=scipy.stats.mstats.winsorize(data, limits=0.05)

		# Create lists
		
		val=[]
		freq=[]
		midi=[]
		new_midi=[]
		oct4_midi=[]

		# Get min amd max
		
		minimum=data.min()
		maximum=data.max()

		# Transform

		for i in data:
			val.append((i-minimum)/(maximum-minimum))
			freq.append((((i-minimum)/(maximum-minimum))*24980)+20)
			midi.append((((i-minimum)/(maximum-minimum))*127))

		# Convert midi scors to integers

		for i in range(0,len(midi)):
		# First round off numbers
			midi[i]=round(midi[i],0)
		# Check for nan floats, then add as string	
			if math.isnan(midi[i]):
				new_midi.append(str(midi[i]))
		# Add all remaining floats as integers
			elif isinstance(midi[i], float):
				new_midi.append(int(midi[i]))


		# Additional part of function, to convert all midi sounds to categories of Octave 4 (between 60 and 71 midi notes)
		for i in new_midi:
			oct4_midi.append(oct4(i))

		return val,freq,new_midi, oct4_midi

def conversion(format, date):
	""" 
	Convert time to UTC in msec
	"""
	utc=int((datetime.datetime.strptime(date, format) - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)
	return utc

def extract_finalTime(trial_end, penult_trial, UTC_end, multiplier):
	""" 
	Fetch final time from Time map file
	trial_diff = Subtract penultimate time from final trial time
	multiply (1000) to get ms 
	Subtract trial total in ms from UTC final time_list
	Multiply trial diff by multiplier to get time in ms increase
	"""
	trial_diff=int(round((trial_end-penult_trial)*1000))
	end_time_ms=int(round((trial_end*1000)))
	data=(UTC_end-end_time_ms)+(trial_diff*multiplier)
	return data

def time(data):
	"""
	Calculate time in msec per Frame-Features
	"""
	inc=(1/30)*1000
	msec=[]
	for i in range(0,len(data)):
		times=inc*i
		times=round(times,0)
		times=int(times)
		msec.append(times)
	return msec

def oct4(data):
	"""
	Convert _midi values into octave 4 midi range
	12 notes per octave
	Increments represent note categories
	e.g., for midi range(0,127) is 128/12 = 11
	"""
	val_inc=.083
	midi_inc=int(11)
	freq_inc=2082

	octave=[]

	if data == 'nan':
		pass
	else:
		if data >=0 and data <= midi_inc:
			octave=60
		elif data > midi_inc and data <= midi_inc*2:
			octave=61
		elif data > midi_inc*2 and data <= midi_inc*3:
			octave=62
		elif data > midi_inc*3 and data <= midi_inc*4:
			octave=63
		elif data > midi_inc*4 and data <= midi_inc*5:
			octave=64
		elif data > midi_inc*5 and data <= midi_inc*6:
			octave=65
		elif data > midi_inc*6 and data <= midi_inc*7:
			octave=66
		elif data > midi_inc*7 and data <= midi_inc*8:
			octave=67
		elif data > midi_inc*8 and data <= midi_inc*9:
			octave=68
		elif data > midi_inc*9 and data <= midi_inc*10:
			octave=69
		elif data > midi_inc*10 and data <= midi_inc*11:
			octave=70
		elif data > midi_inc*11 and data <= midi_inc*12:
			octave=71
	return octave