# For transforming EyeTribe eyetracker
# Author: David Bridges (david.bridges@plymouth.ac.uk)

import pandas as pd
import glob, os, datetime, normz,json
from pandas.io.json import json_normalize

# Create new path for data
path = "C:\\Users\\Research\\Desktop\\data\\"
newpath=(path+"Normed_data\\ET_norm\\")

# Create output paths 
if not os.path.exists(newpath):
    os.makedirs(newpath)

#if file in os.listdr
file_list=[]
for file in os.listdir(path):
	if file.endswith('_ET.json'):
		file_list.append(file)

# Check film file is in folder
film_data='film_output.csv'
if not os.path.isfile(path+film_data):
	print ("Film file not in folder")
	exit()

#Get film start time from film file
film=pd.read_csv(path+film_data, na_values=" ")
film_start=film.loc[0,'UTC_time']
time_format='%Y-%m-%d %H:%M:%S.%f'

# For files in new file list, transform
for files in file_list:
	
	# Create list to append json dictionary data
	conv = []
	for line in open(path+files, 'r'):
		conv.append(json.loads(line)) 
	# Normalize json data into readable format
	data=json_normalize(conv)
	data.to_csv((newpath+files+'.csv'))
	# Remove whitespaces and drop NA data
	data.columns=[x.strip().replace(' ', '') for x in data.columns]
	data=data.dropna(axis=0) 
	
	# For each column, transform unless specified
	for i in data:
		if i =='category' or i == 'request' or i == 'values.frame.fix' or i == 'values.frame.time' or i == 'values.frame.timestamp':
			pass 
		else:
			data[i+'_val'],data[i+'_freq'],data[i+'_midi'],data[i+'_oct4']=normz.transforms(data[i])
	
 	#For each row, create UTC index, and UTC index relative to film start time
	for i in data.index.values:
		data.loc[i,'UTC_time']=normz.conversion(time_format, data.loc[i,'values.frame.timestamp'])
		data.loc[i,'time']=film_start-data.loc[i,'UTC_time']+3600000

	#Find film start time
	data=data.query('time <0 and time > -1839300')
	data.time=data.apply(lambda x: abs(x.time), axis=1)

	# Create subsets of specific times for dog1,dog2,child scene
	child=data.query('time >=893500 and time <=925033')
	dog1=data.query('time >=951500 and time <=974467')
	dog2=data.query('time >=1690600 and time <=1714067')

	# Write to csv
	data.to_csv((newpath+files+"_new.csv"))
	child.to_csv((newpath+files+"_child.csv"))
	dog1.to_csv((newpath+files+"_dog1.csv"))
	dog2.to_csv((newpath+files+"_dog2.csv"))
	