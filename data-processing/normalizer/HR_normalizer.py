# For transforming tech office HR monitor
# Author: David Bridges (david.bridges@plymouth.ac.uk)

import pandas as pd
import glob, os, datetime, normz

# Create new path for data
path = "C:\\Users\\Research\\Desktop\\data\\"
newpath=(path+"Normed_data\\HR_norm\\")

# Create output paths 
if not os.path.exists(newpath):
    os.makedirs(newpath)

#if file in os.listdr
file_list=[]
for file in os.listdir(path):
	if file.endswith('_HR.csv'):
		file_list.append(file)

# Check film file is in folder
film_data='film_output.csv'
if not os.path.isfile(path+film_data):
	print ("Film file not in folder")
	exit()

#Get film start time from film file
film=pd.read_csv(path+film_data, na_values=" ")
film_start=film.loc[0,'UTC_time']
time_format='%Y-%m-%dT%H:%M:%S.%f'

# For files in new file list, transform
for files in file_list:
	data=pd.read_csv(path+files, na_values=" ")
	# Replace white spaces with NaN vals
	data.columns=[x.strip().replace(' ', '') for x in data.columns]

	# For each column, transform unless specified
	for i in data:
		if i =='#PID' or i == 'StartDateTime' or i == 'Event' or i == 'Notes':
			pass 
		else:
			data[i+'_val'],data[i+'_freq'],data[i+'_midi'],data[i+'_oct4'] =normz.transforms(data[i])

	#For each row, create UTC index, and UTC index relative to film start time
	data['UTC_time']=normz.conversion(time_format, data.loc[0,'StartDateTime'])+data['Elapsed_ms']
	data['time']=film_start-data['UTC_time']-39600000

	#Find film start time and select data only for film time
	data=data.query('time <0 and time > -1839300')

	# Create absolute values for time
	data.time=data.apply(lambda x: abs(x.time), axis=1)

	# Create subsets of specific times for dog1,dog2,child scene
	child=data.query('time >=893500 and time <=925033')
	dog1=data.query('time >=951500 and time <=974467')
	dog2=data.query('time >=1690600 and time <=1714067')
	
	# Write to csv
	data.to_csv((newpath+files+"_new.csv"))
	child.to_csv((newpath+files+"_child.csv"))
	dog1.to_csv((newpath+files+"_dog1.csv"))
	dog2.to_csv((newpath+files+"_dog2.csv"))