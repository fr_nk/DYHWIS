# For transforming FL classifier outputs from film file
# Author: David Bridges (david.bridges@plymouth.ac.uk)

import pandas as pd
import glob, os, datetime, normz

# Change working directory
os.chdir("C:\\Users\\Research\\Desktop\\data\\")
# Create new path for data
newpath=("Normed_data\\CF_norm\\")
# Create output paths 
if not os.path.exists(newpath):
    os.makedirs(newpath)

# Get file from folder list
folder_lists=glob.glob('*_CF.csv')

# For files on folder list, add to list with shortened name
file_list=[]
for files in folder_lists:
	file_list.append(files)

# For files in new file list, transform

for files in file_list:
	data=pd.read_csv(files, na_values=" ")

	# Replace white spaces with NaN vals
	data.columns=[x.strip().replace(' ', '') for x in data.columns]

	# For each column, transform unless specified
	for i in data:
		if i =='class' or i == 'Filename':
			pass 
		else:
			print (i)
			data[i+'_val'],data[i+'_freq'],data[i+'_midi'],data[i+'_oct4']=normz.transforms(data[i])

# Add time per frame in ms, from start of film
data['time']=normz.time(data['class'])	

# Write to csv
data.to_csv((newpath+files+"_new.csv"))