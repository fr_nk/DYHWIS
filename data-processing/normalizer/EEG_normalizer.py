# For transforming EEG data
# Author: David Bridges (david.bridges@plymouth.ac.uk)

import pandas as pd
import glob, os, datetime, normz 

# Create new path for data
path = "C:\\Users\\Research\\Desktop\\data\\"
newpath=(path+"Normed_data\\EEG_norm\\")

# Create output paths 
if not os.path.exists(newpath):
    os.makedirs(newpath)

#if file in os.listdr
file_list=[]
for file in os.listdir(path):
	if file.endswith('_EEG.csv'):
		file_list.append(file)

# Check film file is in folder
film_data='film_output.csv'
if not os.path.isfile(path+film_data):
	print ("Film file not in folder")
	exit()

# Get film start time from film file
film=pd.read_csv(path+film_data, na_values=" ")
film_start=film.loc[0,'UTC_time']

# Add start times of data collection
EEG_start=['15.08.16 16.20.36.4608'] # Enter dates manually, e.g., '09.08.16 10.11.10.4608'

if not EEG_start[0]:
	print ("Enter the EEG start times manually")
	exit() 

EEG_file=0 # counter for EEG start times
time_format = '%d.%m.%y %H.%M.%S.%f'

# For files in new file list, transform
for files in file_list:
	data=pd.read_csv(path+files, na_values=" ", delimiter=';')

	# Replace white spaces with NaN vals
	data.columns=[x.strip().replace(' ', '') for x in data.columns]

	# For each column, transform
	for i in data:
		data[i+'_val'],data[i+'_freq'],data[i+'_midi'],data[i+'_oct4']=normz.transforms(data[i])

	# For each row, create UTC index, and UTC index relative to film start time
	for i in data.index.values:
		data.loc[i,'UTC_time']=normz.extract_finalTime(data['Time(s)'].iloc[-1],data['Time(s)'].iloc[-2], normz.conversion(time_format, EEG_start[EEG_file]),i) # ADD END TIME HERE
		data.loc[i,'time']=film_start-data.loc[i,'UTC_time']+3600000
	
	EEG_file=EEG_file+1

	# Create column which identifies fil show time

	#Find film start time
	data=data.query('time <0 and time > -1839300')
	data.time=data.apply(lambda x: abs(x.time), axis=1)
	# Get specific times for dog1,dog2,child scene
	child=data.query('time >=893500 and time <=925033')
	dog1=data.query('time >=951500 and time <=974467')
	dog2=data.query('time >=1690600 and time <=1714067')
	# Write to csv
	data.to_csv((newpath+files+"_new.csv"))
	child.to_csv((newpath+files+"_child.csv"))
	dog1.to_csv((newpath+files+"_dog1.csv"))
	dog2.to_csv((newpath+files+"_dog2.csv"))