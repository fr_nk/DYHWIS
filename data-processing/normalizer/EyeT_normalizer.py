#! Normalizer.py 
# A file for normalising data
# For EyeTribe eyetracker
# Written by David Bridges

import pandas as pd
import numpy as np
import math, glob

# Get list of files

# Open data and remove white spaces
working_fold=("C:\\Users\\Research\\Desktop\\doYouHearWhatISee\\doYouHearWhatISee\\")
files="EyeTribe-Eyetracker.csv"
#working_fold=("C:\\Users\\Research\\Desktop\\doYouHearWhatISee\\doYouHearWhatISee\\test_data\\")

# folder_lists=glob.glob(working_fold+'*.csv')
# file_list=[]
# for i in folder_lists:
# 	file_list.append(i[-20:])

# for files in file_list:
data=pd.read_csv(working_fold+files, na_values=" ")

# Replace white spaces with NaN vals
data.columns=[x.strip().replace(' ', '') for x in data.columns]

# msec= data['Elapsed_ms']
# GSR =data['GSR']
# HR = data['HR']

def transforms (data):
	""" 
	Transform data ebtween 0 and 1 
	"""
	val=[]
	freq=[]
	midi=[]
	new_midi=[]
	# Get min amd max
	minimum=data.min(skipna=True)
	maximum=data.max(skipna=True)

	# Transform
	for i in data:
		val.append((i-minimum)/(maximum-minimum))
		freq.append((((i-minimum)/(maximum-minimum))*24980)+20)
		midi.append((((i-minimum)/(maximum-minimum))*127))

	# Convert midi scors to integers

	for i in range(0,len(midi)):
	# First round off numbers
		midi[i]=round(midi[i],0)
	# Check for nan floats, then add as string	
		if math.isnan(midi[i]):
			new_midi.append(str(midi[i]))
	# Add all remaining floats as integers
		elif isinstance(midi[i], float):
			new_midi.append(int(midi[i]))
	return val,freq,new_midi


data['Pupil_L_val'],data['Pupil_L_freq'],data['Pupil_L_midi']=transforms(data['Pupil_L'])
data['GLX_val'],data['GLX_freq'],data['GLX_midi']=transforms(data['Gaze_LX'])
data['GLY_val'],data['GLY_freq'],data['GLY_midi']=transforms(data['Gaze_LY'])
data['Pupil_R_val'],data['Pupil_R_freq'],data['Pupil_R_midi']=transforms(data['Pupil_R'])
data['GRX_val'],data['GRX_freq'],data['GRX_midi']=transforms(data['Gaze_RX'])
data['GRY_val'],data['GRY_freq'],data['GRY_midi']=transforms(data['Gaze_RY'])

data.to_csv((working_fold+files+"_new.csv"))
