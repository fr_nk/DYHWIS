import numpy as np 
import matplotlib.pyplot as plt
import scipy
from scipy.fftpack import fft

#Select the EEG file
eeg_file = open("P01_EEG.csv_dog2.csv", "r")


#Extract the data from a specific electrode
it = 0
O1_data = []
first_line = True
for line in eeg_file:
	if first_line:
		#To get rid of the first line with labels in the CSV file
		first_line = False
	else:
		#Split the line and select the 9th row (O1 electrode)
		content = line.split(",")[8]
		O1_data.append(float(content))

	print it
	it+=1
eeg_file.close()

time_windows = []
i = 0
#125 recordings per window in order to have 1sec per data point (as recording was done with 125Hz)
window = 125
#For each 1s of data, calculate the amount of alpha wave
while i*window+window<len(O1_data):
	N = window
	# sample spacing
	T = 1.0 / 125.0
	x = np.linspace(0.0, N*T, N)
	y = O1_data[i*window:i*window+window]
	#Generate the Fast Fourier Transform
	yf = fft(y)
	xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
	#For the current time window, calculate the mean for frequency between 7Hz and 12Hz (alpha waves)
	time_windows.append(np.mean(yf[7:12]))
	i+=1
	print i
plt.plot(time_windows)
plt.show()

#write the output in a CSV file
output_file = open("P01_EEG_dog2_Alpha_1S_time_windows.csv","w")
for item in time_windows:
	print float(item)
	output_file.write(str(float(item))+"\n")

output_file.close()
