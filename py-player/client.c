/*
    
Read from a single sensor socket and write to STDOUT and/or file.

author: Thomas Wennekers <Thomas.Wennekers@plymouth.ac.uk>
    
*/



# include <stdio.h> 
# include <stdlib.h>
# include <string.h> 
# include <unistd.h>
# include <time.h>
# include <sys/time.h>
# include <sys/socket.h>    // socket    // why does it not contain  close()   ???
# include <arpa/inet.h>     // inet_addr
 

# define BUFSIZE 256


# define SAVE_THIS 1 



void display_message ( unsigned char *buf )
{
  int i, n;
  unsigned long tick;
  
  n = buf[0];
  printf("%d, ", n );
  buf++; n--;
  
  tick = buf[0]<<24  |  buf[1] << 16 | buf[2] << 8 | buf[3] ;
  printf("%lu, ", tick );
  n-=4;
  buf+=4;
  
  
  for (i=0; i<n/2; i++)    
    printf( "%u, ", buf[2*i]<<8  |  buf[2*i+1] & 0xff );
    
  if ( n != 2*(n/2) )
    printf( "%u", buf[n-1] );
    
  printf( "\n" ); 
  fflush(stdout);
}



void save_message ( unsigned char *buf, FILE *fi )
{
  int i, n;
  unsigned long tick;
  time_t t  = time(NULL);
  struct tm* tm = localtime( &t ); 
  struct timeval tv1;
  char s[64];
  
  gettimeofday( &tv1, NULL );

 // strftime( s, sizeof(s), "%c", tm );
  
  strftime( s, sizeof(s), "%Y-%m-%dT%H:%M:%S", tm );
    
 // fprintf( fi, "%s.%d , ", s, tv1.tv_usec/1000 ); 
  
  fprintf( fi, "%ld , ", (tv1.tv_sec*1000+tv1.tv_usec)/1000 ); 
            
           
  n=buf[0];
  buf++; n--;
  
  tick = ( buf[0]<<24  |  buf[1] << 16 | buf[2] << 8 | buf[3] ) / 1000 ;
  fprintf( fi, "%lu, ", tick );
  n-=4;
  buf+=4;
  
  for (i=0; i<2; i++)    
    fprintf( fi, "%f, ", (buf[2*i]<<8  |  buf[2*i+1] & 0xff) / 1024.0);
    
  for (i=2; i<5; i++)    
    fprintf( fi, "%f, ", (buf[2*i]<<8  |  buf[2*i+1] & 0xff) / 65536.0);
    
  fprintf( fi, "\n" ); 
  fflush( fi );
}

         


void save_message_ ( unsigned char *buf, FILE *fi )
{
  int i, n;
  unsigned long tick;
  time_t t  = time(NULL);
  struct tm* tm = localtime( &t ); 
  struct timeval tv1;
  char s[64];
  
  gettimeofday( &tv1, NULL );

 // strftime( s, sizeof(s), "%c", tm );
  
  strftime( s, sizeof(s), "%Y-%m-%dT%H:%M:%S", tm );
    
 // fprintf( fi, "%s.%d , ", s, tv1.tv_usec/1000 ); 
  
  fprintf( fi, "%ld , ", s, tv1.tv_sec*1000+tv1.tv_usec/1000 ); 
            
  n=buf[0];
  buf++; n--;
  
  tick = buf[0]<<24  |  buf[1] << 16 | buf[2] << 8 | buf[3] ;
  fprintf( fi, "%lu, ", tick );
  n-=4;
  buf+=4;
  
  for (i=0; i<n/2; i++)    
    fprintf( fi, "%u, ", buf[2*i]<<8  |  buf[2*i+1] & 0xff );
    
  if ( n != 2*(n/2) )
    fprintf( fi, "%u", buf[n-1] );
    
  fprintf( fi, "\n" ); 
  fflush( fi );
}

         
         
         
void display_bytes_x ( char *buf, int n )
{
  int i;
  printf("x %d", n );
  
  for (i=0; i<n; i++)
    printf( "%x ", buf[i] & 0xff );
  printf( "\n" ); 
}

void display_bytes_u ( char *buf, int n )
{
  int i;
  printf("u %d", n );
  
  for (i=0; i<n; i++)
    printf( "%u ", buf[i] & 0xff );
  printf( "\n" ); 
}

void display_bytes ( char *buf, int n )
{
  int i;
  printf("d %d", n );
  
  for (i=0; i<n; i++)
    printf( "%d ", buf[i] & 0xff );
  printf( "\n" ); 
}
         
         
         

         
int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;
    char buf[BUFSIZE];
    int bufpos = 0;
    char filename[256];
    FILE *fi;
    int length = 1;
    char servername[100]; 
    int port = 301;
    struct tm *tm; 
    time_t t;
    char s[64];
    struct timeval tv1;
    long ttime;
    
    
        
    int saving = SAVE_THIS;
        
        
        
    printf("argc %d\n", argc );
   
    if ( argc == 1 )
    {
      printf("Usage: %s IP [ bytes [ port ] ] \n", argv[0] );
      exit(0);
    }
    
   //  sprintf( servername, "192.168.43.%s", argv[1] );
    sprintf( servername, "%s", argv[1] );
        
    if (argc>2)
      length = atoi( argv[2] );
      
    if (argc>3)
      port   = atoi( argv[3] );
    
    printf("Connecting to %s at port %d for messages of size %d \n", servername, port, length ); 
    
    
    
    // Create socket
    sock = socket( AF_INET , SOCK_STREAM , 0 );
    if (sock == -1)
    {
        perror("Could not create socket");
        return -1;
    }
     
    // server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_addr.s_addr = inet_addr(servername);
    server.sin_family = AF_INET;
    server.sin_port = htons( port );
 
    // Connect to remote server
    
    if (1)
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return -1;
    }
     
    
    
    // make this simpler ... 
    
    gettimeofday( &tv1, NULL );
    ttime = tv1.tv_sec*1000000 + tv1.tv_usec ;
    printf( "Current system time : %ld \n", ttime );
    
    t  = time(NULL);
    tm = localtime( &t ); 
    strftime( s, sizeof(s), "%c", tm );
    printf( "%s + %lu us\n", s, tv1.tv_usec ); 
    sprintf( filename, "%s-%d-%ld", argv[1], port, ttime );
    
    if(1)
      sprintf( filename, "out" );
      
    printf( "Storing data to file '%s'\n", filename ); 
    fi = fopen( filename , "w" );
    
    fprintf( fi, "## Connecting to %s at port %d for messages of size %d\n", servername, port, length ); 
    fprintf( fi, "## Current system time : %ld \n", ttime );
    fprintf( fi, "## %s + %lu us\n", s, tv1.tv_usec ); 
    fprintf( fi, "## Storing data to file '%s' (this file)\n", filename ); 


    //  strftime( s, sizeof(s), "%Y-%m-%dT%H:%M:%S.%f", tm );
      

    // keep communicating with server
    while(1)
    {
        int nreceived, i;

        if( (nreceived = recv( sock, &buf[bufpos], BUFSIZE-bufpos, 0)) < 0 )
          break;
        
        bufpos += nreceived;
   //     printf("received %d  bufpos %d length %d \n", nreceived, bufpos, 10 );
                
                
        while ( bufpos >= length )
        {

    //      display_bytes( buf, length );
    //      display_bytes_u ( buf, length ); 
    //      display_bytes_x ( buf, length ); 
          display_message( buf );
          
          if ( saving )
          //  fwrite( buf, 1, length, fi );
            save_message ( buf, fi ) ;   // ASCII
          
          for ( i=0; i<bufpos-length; i++) 
            buf[i] = buf[length+i];
          bufpos-=length;
          
        }
        
       // usleep( 500000 ) ;
    }
     
    fclose( fi );
     
    // close(sock); //  function does not exist in socket.h ???
    // shutdown( sock, 2 ); 
    close( sock );
    return 0;
}
