
/* TODO
 *  - do we need Wire and I2Cdev ?   >>>  I2Cdev looks unused indeed !
 *  - do we use fast HW SPI    >>> I suspect so, after looking into the SPI lib code
 *  - use Wire-functions instead of the I2C functions defined in this file 
 *         setClock(uint32_t);    write( ..      requestFrom( 
 *  - clean up 1Wire
 *  - clean up native stuff, blinking etc 
 *  - use MPU lib ???    low-pass / highpass???  this had little effect in "glove" code 
 *  - blink? 
 *  -  other ??? -- things to do / improve 
 */





#include <OneWire.h> 
#include <Wire.h>
#include <SPI.h>
#include <Ticker.h>
#include <ESP8266WiFi.h>

# define MODE_FINGER  0x01
# define MODE_STW     0x02  // Space To Wonder




// Compile time switches
# define DEBUG         1   // verbose print enabled (1) or disabled (0)  --  0 saves a bit of memory
# define DEVICE_SERIAL 1   // serial device enabled (1) or disabled (0)
# define DEVICE_WIFI   1   // WiFi enabled (1) or disabled (0)


///////////////////////////////
// Configuration

# define OPERATION_MODE (MODE_STW)  // what mode is this device going to operate in? Could be one of the MODE_* variables above.

# define DEVICE_PORT 301   // PORT this device is sending through. Suggested is 301
# define MAX_FREQ    50   // maximum frequency to send packets. Should be between 0.1 and 400



///////////////////////////////



#if (OPERATION_MODE & MODE_STW)
  # define NW_SSID "IMPROV"     // Network configuration (WPA)
  # define NW_KEY  "improv321"
#endif
#if (OPERATION_MODE & MODE_FINGER)
  # define NW_SSID "s2m"     // Network configuration (WPA)
  # define NW_KEY  "cognovoworkshop"
#endif



# define   DATA_BUF_LEN 64 

# define SERIAL_BAUDRATE  115200 // 9600  115200

#if DEBUG == 1
  #define SerialOut( _x )  if ( DEVICE_SERIAL == 1 ) Serial.print( _x ) 
#else
  #define SerialOut( _x )
#endif

# define HELLO            "\n\nIs this the real world?\n"
# define CREDENTIALS      "Copyright: twennekers@plymouth.ac.uk\n"
# define WARRANTY         "Use at your own risk. No warranty of any kind.\n\n"
 
 
 
// stuff on native pins  && one-wire bus

# define DEVICE_NATIVE_MASK  0x000F
# define DEVICE_1WIRE        0x0001
# define DEVICE_ADC0         0x0002
# define DEVICE_BUZZER       0x0004
# define DEVICE_BLUE_LED     0x0008


 # define DEVICE_NATIVE  ( DEVICE_ADC0 )  // what do we use ?
// # define DEVICE_NATIVE   ( 0x00 )  // what do we use ?


// the blink stuff is quite a mess as the pins are possibly doubly used ...  CLEAN UP ??? 

// # define DEVICE_LED_BUILTIN (!DEVICE_SERIAL)   // Red LED is connected to D10/Tx
# define DEVICE_LED_BUILTIN  0
//# define BLINK_LED D0             // red should blink  // can't be used if DEVICE_SERIAL is required
# define BLINK_LED D4               // blue should blink  GPIO02 // ??? might be shared with something else 

# define BUZZER_PIN          D3 


// the following needs initialisation and duration/frequency parameters ???
// idea is that this macro plays a sound at a certain freq/duration if a 
// speaker or piezo is attached at BUZZER_PIN
// how does this actually work on ESP???
// # define BUZZER_PIN    ??
// # define BUZZ( _x ) if ( DEVICE_BUZZER ) tone (_x) 



// stuff on the 1-Wire bus 

# define DEVICE_1WIRE_PIN        2     // D4/GPIO02 this pin has the blue led

// thought one could use the DS18x20, but a reading needs 100-700ms

// .. OneWire left out for now; unless another device becomes available ... 



// stuff on the SPI bus 

# define DEVICE_SPI_MASK  0x00F0
# define DEVICE_MCP3008   0x0010       // MCP3008 10bit 8ch ADC 

// SPI pins 
#define CLOCK_PIN  D5  // 2
#define MISO_PIN   D6  // 5
#define MOSI_PIN   D7  // 4
#define CS_PIN     D8  // 14



// stuff on the I2C bus // uses I2C hardware with fixed pins 

# define DEVICE_I2C_MASK       0x0F00
# define DEVICE_GYRO0          0x0100
# define DEVICE_GYRO1          0x0200
# define DEVICE_COMPASS        0x0400
# define DEVICE_PCF8574        0x0800       // 8ch GPIO extender

# define DEVICE_GYRO0_ADDRESS    0x68
# define DEVICE_GYRO1_ADDRESS    0x69
# define DEVICE_COMPASS_ADDRESS  0x1E
# define DEVICE_PCF8574_ADDRESS  0x38


// what do we actually have connected ? 

#if (OPERATION_MODE & MODE_STW)
  # define DEVICE_MASK    ( DEVICE_MCP3008 )
#endif
                    //  | DEVICE_GYRO0 | DEVICE_GYRO1
                    //  | DEVICE_PCF8574  DEVICE_MCP3008 | DEVICE_GYRO0 | DEVICE_NATIVE


                          // comments on the next lines refer to the experimental setup
                          // the devices for cognovo "usually" have A0 and A2 enabled 
                          //                         for breathband and finger LDR
# define MCP3008_A0 0x01  // diode (for temperature measurement)
# define MCP3008_A1 0x02  // LDR   (light / seeing finger)
# define MCP3008_A2 0x04
# define MCP3008_A3 0x08
# define MCP3008_A4 0x10
# define MCP3008_A5 0x20
# define MCP3008_A6 0x40  // GSR    (some problems with input impedance; not very stable measurements)
# define MCP3008_A7 0x80  // piezo  (tap or breath ? )


// which ADC channels to read 
// # define DEVICE_MCP3008_MASK  ( MCP3008_A0 | MCP3008_A1 | MCP3008_A2 )  
# if (OPERATION_MODE & MODE_STW)
  # define DEVICE_MCP3008_MASK  ( MCP3008_A2 )  
# endif
# if (OPERATION_MODE & MODE_FINGER)
  # define DEVICE_MCP3008_MASK  ( MCP3008_A0 | MCP3008_A1 | MCP3008_A2 )  
# endif

SPISettings MCP3008( 2000000, MSBFIRST, SPI_MODE0 ); // << check speed; 2MB seem to work fine; 4MB may work

                          // these comments are for the test setup ..
# define PCF8574_P0 0x01
# define PCF8574_P1 0x02
# define PCF8574_P2 0x04  // red LED
# define PCF8574_P3 0x08  // pager via FET
# define PCF8574_P4 0x10  // input button
# define PCF8574_P5 0x20
# define PCF8574_P6 0x40
# define PCF8574_P7 0x80

// data direction mask;  high == input
# define DEVICE_PCF8574_DDR_MASK  ( PCF8574_P4 )





// MPU6050 gyro0( 0x68 ), gyro1( 0x69 );   // AD0 low  = 0x68   AD0 high = 0x69


WiFiServer data_conn( DEVICE_PORT );                // data stream
WiFiClient data_client;

 
Ticker ticker;
int ticked = 0;
float tick_interval = 1.0 / MAX_FREQ;  // ca 30Hz with ADC only, 11Hz maximum with one ?  6-7 with 2 gyros ????



void blink( int pin ) 
{
  if ( DEVICE_LED_BUILTIN )  // blink a bit 
  {
    int state = digitalRead( pin );  // get the current state of GPIO1 pin = D10 = Tx
    digitalWrite( pin, !state);     // set pin to the opposite state
  }
}


// it is suggested to do all the work in the Arduino loop--routine not to interfer with 
// ESP-intrinsic wireless and whatever else
// therefore, the tick() routine is basically empty 

void do_tick ()
{
  blink( BLINK_LED ); // waste of energy; testing only ...
  ticked = 1;
}



// only for testing at the moment; conversion rate is too low (10Hz at 9 bit )
uint16_t read_DS18x20_Temperature()
{
  OneWire  ds(DEVICE_1WIRE_PIN);  // that's D4 (pin with blue LED has a pull up resistor already ..)
  byte addr[8];
  byte data[5];
  int i;
  
//  ds.reset_search(); // ? 
  if ( !ds.search(addr)) {
    SerialOut("No OneWire device found.");
    return 0;
  }

  // CFG register:  X R1 R0 1  1 1 1 1
  
  //  R1 R0  Bits 
  //   0  0   9    94ms
  //   0  1  10   189ms
  //   1  0  11   375ms
  //   1  1  12   750ms 
  
  
  ds.reset();
  ds.select(addr);
  ds.write(0x4E);   // Write scratch pad 
  ds.write(0x00);   // TH
  ds.write(0x00);   // TL
  ds.write(0x1F);   // CFG
  
  ds.reset();
  ds.select(addr);
  ds.write(0x44);   // start conversion 
  

  delay(100);    // THIS COULD INTERRUPT ESP8266 WIRELESS ????
                 //   better set a flag and collect results later 
    
  ds.reset();
  ds.select(addr);    
  ds.write(0xBE);   // Read Scratchpad
  
                                       //   0    1    2       3    4   8 
  for ( i = 0; i < 5; i++) {           // TLSB TMSB TH/UB1 TL/UB2 CFG  CRC
    data[i] = ds.read();
    //SerialOut(data[i]);
    //SerialOut(" ");
  }
  ds.reset();
  SerialOut ("\n");
  
  int16_t raw = (data[1] << 8) | data[0] ;
  byte cfg = (data[4] & 0x60);
  // at lower res, the low bits are undefined, so let's zero them
  if (cfg == 0x00) raw = raw & ~7;       // 9 bit resolution, 93.75 ms
  else if (cfg == 0x20) raw = raw & ~3;  // 10 bit res, 187.5 ms
  else if (cfg == 0x40) raw = raw & ~1;  // 11 bit res, 375 ms
  // default is 12 bit resolution, 750 ms conversion time

  SerialOut( (float)raw / 16.0 ); SerialOut ("\n"); 
  
  return raw ; 
}
  

uint16_t ADC_Read( int channel ) 
{    
  SPI.beginTransaction ( MCP3008 ) ; // MCP3008 is a SPISettings object
  
  digitalWrite (CS_PIN, LOW );
  SPI.transfer ( 0x01 );
  uint8_t r1 = SPI.transfer((channel + 8) << 4);
  uint8_t r2 = SPI.transfer(0);
  digitalWrite (CS_PIN, HIGH); 
  
  SPI.endTransaction ();

 // SerialOut( r1 ); SerialOut( " " ); SerialOut( r2 ); SerialOut( " " );
  
  return ((r1 & 3) << 8) + r2;
}


void I2C_Write_Byte( int device_addr, uint8_t d )
{
      Wire.beginTransmission( device_addr );
      Wire.write( d );  
      Wire.endTransmission( true );   // ????  true what ????   
}

void I2C_Write_Two_Bytes( int device_addr, uint8_t d1, uint8_t d2 )
{
      Wire.beginTransmission( device_addr );
      Wire.write( d1 );
      Wire.write( d2 );  
      Wire.endTransmission( true );   // ????  true what ????   
}

uint8_t I2C_Read_Byte( int device_addr )
{
  // Wire.requestFrom ( DEVICE_PCF8574_ADDRESS, 1 );
  Wire.requestFrom ( device_addr, 1 );
  if (Wire.available()) 
     return Wire.read();
}


void read_I2C_Data( int device_addr, int addr, int n )
{
  Wire.beginTransmission ( device_addr );
  Wire.write ( addr );
  Wire.endTransmission ( false ); // <<< ??  false what ???
  
  Wire.requestFrom( device_addr, n, 1 );  // <<< what's the 1 ??
  
  // read the stuff 

  if ( Wire.available() == n )   // else ???? 
  {
    int i;
 //   int16_t  x = Wire.read();
    for (i=0; i<n; i++)
      SerialOut( Wire.read() ); SerialOut( "\n" );
  }
}

void read_I2C_Data( int device_addr, int addr, uint8_t * buf, int n )
{
  Wire.beginTransmission ( device_addr );
  Wire.write ( addr );
  Wire.endTransmission ( false ); // <<< ??  false what ???
  
  Wire.requestFrom( device_addr, n, 1 );  // <<< what's the 1 ??
  
  if ( Wire.available() == n )   // else ???? 
  {
    int i;
    for (i=0; i<n; i++)
       buf[i] = Wire.read() ;  // what does this read?? 1 byte ?? more ?? 
  }
}


void print_buffer_as_shorts ( uint8_t * buf, int n )
{
  int i;
  for (i=0; i<n/2; i++)
  { 
    SerialOut( ((uint16_t)buf[2*i] << 8) | buf[2*i+1] ); 
    SerialOut( " " ); 
  }
  SerialOut( "\n" );
}



int pack_time( uint32_t microtime, uint8_t*buf )
{
                                         //   up to 
  buf[0] = (microtime>>24) & 0x000000ff; //  ~  1.2h ~ 71min
  buf[1] = (microtime>>16) & 0x000000ff; //  ~ 16.7s
  buf[2] = (microtime>>8) & 0x000000ff;  //  ~  65ms 
  buf[3] =  microtime    & 0x000000ff;   //    256us

/*
  buf[0] = 1; //  ~  1.2h ~ 71min
  buf[1] = 2; //  ~ 16.7s
  buf[2] = 3;  //  ~  65ms 
  buf[3] = 4;   //    256us

  SerialOut( microtime ); SerialOut( " " ) ;
  SerialOut( buf[0] ); SerialOut( " " ) ;
  SerialOut( buf[1] ); SerialOut( " " ) ;
  SerialOut( buf[2] ); SerialOut( " " ) ;
  SerialOut( buf[3] ); SerialOut( " " ) ;
*/
  return 4;  // bytes packed 
}


  

void setup()
{
  int i;
  
  
  if ( DEVICE_SERIAL == 1 )                  // do we use the Serial?
  {
    
    Serial.begin( SERIAL_BAUDRATE );
    
    delay(3000);                             // make sure serial is connected

  //  too long a delay here .. wifi seems to stop connecting ..... 
 //   for ( i=0; i<10; i++ )
 //   {
 //     delay(500);                          
 //     yield(); 
 //   }
 
    SerialOut ( HELLO );     
    SerialOut ( CREDENTIALS );
    SerialOut ( WARRANTY );
  }
    
  if ( DEVICE_MASK & DEVICE_SPI_MASK )  // anything on the SPI bus ?
  {
  
    pinMode( CS_PIN, OUTPUT );
    digitalWrite( CS_PIN, HIGH );  // LOW for SPI select 

    SPI.begin();
 //  SPI.setHwCs( true );  // ???  doesn't work ??? 

 
 //   SPI.beginTransaction( SPISettings( 1000000, MSBFIRST, SPI_MODE0 ) ); // we may need to end the transaction too ???
       // these may not be the best settings for all SPI devices ... 
    
 // SPI.setBitOrder(MSBFIRST);
 // SPI.setDataMode(SPI_MODE0);
 // SPI.setFrequency(1000000);          // test higher frequencies; especially if impedance > 1k
                                        // MCP3008 has 200ksamples max at 5V ; 2MHz , 4MHz?, 8MHz ??
                                        

  }
  

  if ( DEVICE_MASK & DEVICE_I2C_MASK )  // anything on the I2c bus ?   
                                          // what's the default frequency ???? 
                                          // use a more specific library for the gyros?
  {
    Wire.begin();
    
    if ( DEVICE_MASK & DEVICE_GYRO0 ) 
      I2C_Write_Two_Bytes( DEVICE_GYRO0_ADDRESS, 0x6B, 0x00 ) ;

    if ( DEVICE_MASK & DEVICE_GYRO1 )
      I2C_Write_Two_Bytes( DEVICE_GYRO1_ADDRESS, 0x6B, 0x00 ) ;
     
    if ( DEVICE_MASK & DEVICE_COMPASS ) 
       I2C_Write_Two_Bytes( DEVICE_COMPASS_ADDRESS, 0x02, 0x00 ) ;
    
    if ( DEVICE_MASK & DEVICE_PCF8574 )
      I2C_Write_Byte( DEVICE_PCF8574_ADDRESS, DEVICE_PCF8574_DDR_MASK ) ;
  }
    
  if ( DEVICE_MASK & DEVICE_NATIVE_MASK ) 
  {

   //   ...  clean up ???????????
  
    // fade a LED on D3 on and off    WORKS
    /*
    for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 5) {
      // sets the value (range from 0 to 255):
      analogWrite( BUZZER_PIN , fadeValue );
      delay(30);
    }
    for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 5) {
      // sets the value (range from 0 to 255):
      analogWrite( BUZZER_PIN , 255-fadeValue );
      delay(30);
    }
    */
  
    // tone ( BUZZER_PIN, 1000, 10000 ) ;   // nothing to hear; is the BUZZER DEAD ???
  
    if ( DEVICE_LED_BUILTIN )
      pinMode( BLINK_LED, OUTPUT );
     
    // read_DS18x20_Temperature();   // quite slow .. needs pushing request and collecting result later 
    
  }
  
  // .. any other initialisations ???
  // .. 
  
  
  // WiFi initialisations 
  
  if (DEVICE_WIFI)
  {
    // Connect to WiFi network
    SerialOut( "Connecting to ");
    SerialOut( NW_SSID ); 

    WiFi.mode(WIFI_STA);        // ?? need AP ? 

    WiFi.begin( NW_SSID, NW_KEY );

    while (WiFi.status() != WL_CONNECTED)
    {
      blink( BLINK_LED );
      delay(500);
      SerialOut(" .");
    }
  
    // Start the data connection
    data_conn.begin();
    data_conn.setNoDelay(true);
    SerialOut( "\nData connection started\n" );

    SerialOut("\n\n WiFi connected: ");
    SerialOut( WiFi.localIP() );
    SerialOut( "\n\n" );
    
  }
  
  ticker.attach( tick_interval, do_tick ); 
  
}

  
void loop()
{

  // timer interrupt ? .. perhaps put this into a separate function .. 
  if ( !ticked ) 
    return; 
  ticked = 0;
  
  uint8_t    data_buf[DATA_BUF_LEN];
  uint8_t    data_buf_pos   = 1; // byte 0 is for size of message inserted later
    

  // get the current time ... wraps around after ca 70 minutes -- make sure this does not cause faults ??? 
  uint32_t microtime = micros() ;

  // pack the time into the buffer 
  data_buf_pos += pack_time( microtime, &data_buf[data_buf_pos] ) ;
  
  // work on I2C bus : Gyro(s), compass, GPIO extender, ...
  
  if ( DEVICE_MASK & DEVICE_I2C_MASK )
  {
    if ( DEVICE_MASK & DEVICE_GYRO0 ) 
    {
      read_I2C_Data( DEVICE_GYRO0_ADDRESS, 0x3B, 
                     &data_buf[data_buf_pos], 14 );
      print_buffer_as_shorts( &data_buf[data_buf_pos], 14 );
      data_buf_pos += 14;
    }
     
    if ( DEVICE_MASK & DEVICE_GYRO1 ) 
    {
      read_I2C_Data( DEVICE_GYRO1_ADDRESS, 0x3B, 
                     &data_buf[data_buf_pos], 14 );
      print_buffer_as_shorts( &data_buf[data_buf_pos], 14 );
      data_buf_pos += 14;
    }
     
    if ( DEVICE_MASK & DEVICE_COMPASS ) 
    {
      read_I2C_Data( DEVICE_COMPASS_ADDRESS, 0x03, 
                     &data_buf[data_buf_pos], 6 );
      print_buffer_as_shorts( &data_buf[data_buf_pos], 6 );
      data_buf_pos += 6;
    }
    
    if ( DEVICE_MASK & DEVICE_PCF8574 ) 
    {
      int data = I2C_Read_Byte( DEVICE_PCF8574_ADDRESS );
      SerialOut( data ); SerialOut( "\n" );
      data_buf[data_buf_pos++] =    0;   // make each data item 2bytes long 
      data_buf[data_buf_pos++] = data;

      // testing, debugging, and example ...  mostly unused at the moment (Aug 2016)
      I2C_Write_Byte( DEVICE_PCF8574_ADDRESS, 
                         ( data ^ (PCF8574_P2|PCF8574_P3) ) | DEVICE_PCF8574_DDR_MASK );   
               // PCF8574_P3 is the LED in the test setup; we or-| with DDR_MASK to keep the input pins high          
    } 
  }

 
  // work on SPI bus : 8ch ADC

  if ( DEVICE_MASK & DEVICE_SPI_MASK ) 
  {
    if ( DEVICE_MASK & DEVICE_MCP3008 )
    {
      int i;
      for ( i = 0; i < 8; i++ )
        if ( (1<<i) & DEVICE_MCP3008_MASK ) 
        {
          uint16_t val = 32 * ADC_Read( i ) ; // reads 10bit numbers; scale up to 15 
          
          data_buf[data_buf_pos] = (val>>8) & 0x00ff;
          data_buf[data_buf_pos+1] = val & 0x00ff;
          data_buf_pos+=2;
          
          SerialOut ( val ); SerialOut ( " " );

        }
        SerialOut( "\n" );
    }
    // any other SPI devices ?? ..
  }

  // work on any other native pins : in-built ADC, buzzer, ...   

  if ( DEVICE_MASK & DEVICE_NATIVE_MASK ) 
  {

    if ( DEVICE_MASK & DEVICE_ADC0)  // read inbuilt ADC0 ?
    {
      uint16_t val =  32 * analogRead(A0) ;     // reads 10bit numbers; scale up to 15
      data_buf[data_buf_pos++] = (val>>8) & 0x00ff;   // check this is the same order as for the gyro data 
      data_buf[data_buf_pos++] = val & 0x00ff;
    }

    // buzzer, pager 

    // .. 


    // work on 1Wire bus : e.g. temperature sensors (too slow!)

    // read_DS18x20_Temperature();

    // .. 
  
  }



 
  // check for WiFi data connections 
  // and transmit available data if connected
   
  if  (DEVICE_WIFI)
  {
    if ( data_client.connected() > 0 )
    {
      SerialOut( "send " ); SerialOut( data_buf_pos ); SerialOut( " bytes\n" );
      
      data_buf[0] = (uint8_t)data_buf_pos ;          // number of bytes to send
      data_client.write( (const uint8_t *) data_buf, data_buf_pos );
    } 
    else if (data_client = data_conn.available()) 
    {
       // DO NOTHING; just connect 
    }
  }
}


