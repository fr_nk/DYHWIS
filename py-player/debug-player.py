#!/usr/bin/env python3

"""
debug-Player -- send SynOSCopy messages in a loop to allow synthesizer
debugging.

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import random
import time
import argparse
import math
from SynOSCopy.Bundle import *

def play_random():
    while 1:
        note = random.randint(30,80)
        volume = random.random()
        velocity = random.random()
        pan = random.random()

        bndl = Bundle('127.0.0.1', 8000)

        if random.random() > 0.3:
            bndl.add_voice_on(midi_note=note, velocity=velocity )
        if random.random() > 0.5:
            bndl.add_volume(volume)
        if random.random() > 0.7:
            bndl.add_pan(pan)

        if random.random() > 0.3:
            bndl.add_voice_on(midi_note=note, velocity=velocity, voice_id=2)
        if random.random() > 0.5:
            bndl.add_volume(volume, voice_id=2)
        if random.random() > 0.7:
            bndl.add_pan(pan, voice_id=2)


        length = random.randint(20, 1000)

        now = time.time()
        next_time = now + length /1000.

        cur_time = time.time()
        while time.time() < next_time:
            time.sleep(0.001)
        bndl.send()

def play_heart():
    i = 0
    while 1:
        i = (i + 1) % 50
        if i < 10:
            freq = 220
        else:
            freq = i * 20

        p1 = (i % 13) / 13.0
        p2 = (i % 33) / 33.0
        print(p1, p2)


        bndl = Bundle('127.0.0.1', 8000)
        bndl.add_voice_on(frequency=freq, velocity = p2)
        bndl.add_volume(volume=p1)

        next_time = time.time() + 250 /1000.
        while time.time() < next_time:
            time.sleep(0.001)
        bndl.send()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--type"
        , default="random"
        , help="type of signals to send")
    arguments = parser.parse_args()

    if arguments.type == "random":
        play_random()
    elif arguments.type == "heart":
        play_heart()
    
