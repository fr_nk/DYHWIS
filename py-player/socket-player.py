#!/usr/bin/env python3

"""
Read from a single sensor socket and sends values out as OSC.

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import argparse
import logging
import signal
import sys
from SynOSCopy.Sensor import *
from SynOSCopy.Bundle import *

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)

def signal_handler(signal, frame):
    s.close()
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--sensorip"
        , default="192.168.43.220"
        , help="IP address of the sensor to read")
    parser.add_argument(
        "--sensorport"
        , default=301
        , help="Port to connect to sensor")
    parser.add_argument(
        "--synthip"
        , default="127.0.0.1"
        , help="IP address of the synthesizer, defaults to localhost")
    parser.add_argument(
        "--synthport"
        , default=8000
        , help="Port of the synthesizer, defaults to 8000")
    parser.add_argument(
        "--synthid"
        , default=1
        , help="ID of the synthesizer, defaults to 1")
    parser.add_argument(
        "--startvoice"
        , default=1
        , help="ID of the first voice, defaults to 1")
    parser.add_argument(
        "--outfile"
        , default=None
        , help="Save sensor data to file")
    parser.add_argument(
        "--silent"
        , dest='silent'
        , action='store_true'
        , default=False
        , help="suppress output to stdout")
    arguments = parser.parse_args()

    logging.info(
        "Connecting to sensor %s:%d" % (arguments.sensorip, arguments.sensorport))

    try:
        s = Sensor(arguments.sensorip, arguments.sensorport)
    except OSError:
        print("Could not connect to device with IP %s" % (arguments.sensorip))
        sys.exit(1)

    if arguments.outfile is not None:
        s.set_filename(arguments.outfile)

    print("Press CTRL-C to end application")

    voice1 = arguments.startvoice

    for line in s.get_measures(as_float=True):
        bndl = Bundle(arguments.synthip, arguments.synthport)
        # This is where the mapping from measures (array of values) to Bundle is
        # happening.
        if not arguments.silent:
            print(line)
        ll = len(line) - 2
        for rounds in range(0, ((ll -1) // 4) + 1):
            cpos = rounds*4
            if ll == (cpos +1):
                bndl.add_voice_on(synth_id=arguments.synthid, voice_id=voice1 + rounds, osc_note=line[cpos + 2])
            else:
                bndl.add_voice_on(synth_id=arguments.synthid, voice_id=voice1 + rounds, osc_note=line[cpos + 2], velocity=line[cpos + 3])
                if ll >= (cpos + 3):
                    bndl.add_volume(synth_id=arguments.synthid, voice_id=voice1 + rounds, volume=line[cpos + 4])
                if ll >= (cpos + 4):
                    bndl.add_pan(synth_id=arguments.synthid, voice_id=voice1 + rounds, pan_lr=line[cpos + 5])
        bndl.send()

