#!/usr/bin/bash

echo "You should be connected to the WiFi 'IMPROV' and all 6 sensor devices should be turned on"

if [ ! -d data ]; then
  mkdir data
fi

timestamp=`date +"%Y-%m-%dT%H-%M-%S.%N"` 

PYTHON="/usr/bin/env python3"

($PYTHON socket-player.py --sensorip 192.168.0.151 --silent --outfile "data/$timestamp-01.csv") &
($PYTHON socket-player.py --sensorip 192.168.0.152 --silent --outfile "data/$timestamp-02.csv") &
($PYTHON socket-player.py --sensorip 192.168.0.153 --silent --outfile "data/$timestamp-03.csv") &
($PYTHON socket-player.py --sensorip 192.168.0.154 --silent --outfile "data/$timestamp-04.csv") &
($PYTHON socket-player.py --sensorip 192.168.0.155 --silent --outfile "data/$timestamp-05.csv") &
($PYTHON socket-player.py --sensorip 192.168.0.156 --silent --outfile "data/$timestamp-06.csv") &

wait
echo "Finished"
