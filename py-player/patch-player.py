#!/usr/bin/env python3

"""
OSC-Player using a patchbay file -- read CSV files and play them as OSC as
defined in a patchbay file

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import argparse
import random
import time
import csv
import logging
from SynOSCopy.Bundle import *
from SynOSCopy.Patchbay import *
from SynOSCopy.Tracker import *

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ip"
        , default="127.0.0.1"
        , help="The ip of the OSC server")
    parser.add_argument(
        "--port"
        , type=int
        , default=8000
        , help="The port the OSC server is listening on")
    parser.add_argument(
        "--datadir"
        , default="../example-data/"
        , help="The path where all playlist files are stored")
    parser.add_argument(
        "--patch"
        , default="../example-data/patchbay-example.csv"
        , help="The patchbay file to be used")
    parser.add_argument(
        "--start"
        , default=0
        , type=int
        , help="starting time in ms after patch")
    arguments = parser.parse_args()

    logging.info(
        "Sending messages to {}:{}".format(arguments.ip, arguments.port))

    starttime = time.time()

    p = Patchbay(arguments.patch, arguments.datadir)

    t = Tracker(p.track, arguments.start)
    

    t.play()



