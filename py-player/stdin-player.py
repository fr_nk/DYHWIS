"""
Read from STDIN and send them out as OSC. This code is the basis for a live
performance in the "Let's improv it" project of ColLaboratoire.
https://collaboratoire.cognovo.eu/projects#p6

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import sys
import time
from SynOSCopy.Bundle import *

k = 0
try:
    buff = ''
    while True:
        buff += sys.stdin.read(1)
        if buff.endswith('\n'):
            bndl = Bundle("127.0.0.1", 8000)
            # print(buff[:-1])
            try:
                bndl.add_voice_on(frequency=int(buff.split(",")[5]))
            except:
                pass
            bndl.send()

            buff = ''
            k = k + 1
except KeyboardInterrupt:
   sys.stdout.flush()
   pass
print(k)
