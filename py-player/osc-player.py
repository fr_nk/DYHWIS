"""
OSC-Player -- read CSV files and play them as OSC

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""
import argparse
import random
import time
import csv
import logging
from SynOSCopy.Bundle import *

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ip"
        , default="127.0.0.1"
        , help="The ip of the OSC server")
    parser.add_argument(
        "--port"
        , type=int
        , default=8000
        , help="The port the OSC server is listening on")
    parser.add_argument(
        "--input"
        , default="../example-data/scale.csv"
        , help="The input file to be played")
    arguments = parser.parse_args()

    logging.info(
        "Sending messages to {}:{}".format(arguments.ip, arguments.port))

    starttime = time.time()
    with open(arguments.input) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            next_time = starttime + (float(row['time']) / 1000.)

            bndl = Bundle(arguments.ip, arguments.port)
            bndl.add_volume(volume = 1)
            bndl.add_volume(voice_id=3, volume=1)
            bndl.add_voice_on(midi_note=int(float(row['v1'])) )
            # bndl.add_voice_on(voice_id=2, midi_note=int(float(row['v2'])))
            # bndl.add_volume(voice_id=1, volume=(float(row['v3'])-925)/320)
            # bndl.add_volume(voice_id=2, volume=(float(row['v3'])-925)/320)

            cur_time = time.time()
            while cur_time < next_time:
                time.sleep(0.01)
                cur_time = time.time()
            bndl.send()

    bndl = Bundle(arguments.pi, arguments.port)
    time.sleep(1)
    bndl.add_volume(0)
    bndl.add_volume(voice_id=2, volume=0)
    bndl.add_volume(voice_id=3, volume=0)
    bndl.send()

