"""
Library to send out modified SynOSCopy messages from a simpler interface.

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

from pythonosc import osc_message_builder, osc_bundle_builder
from pythonosc import udp_client

import logging

class Bundle(object):
    """
    A bundle consists of a number of messages that are sent at the same time.
    """


    def __init__(self, client_ip="127.0.0.1", client_port=8000, time=osc_bundle_builder.IMMEDIATELY):
        """
            The constructor needs to have a client specified. Potentially the
            time can be overwritten.
        """
        self.client = udp_client.UDPClient(client_ip, client_port)
        self.playtime = time
        self.bndl = osc_bundle_builder.OscBundleBuilder(self.playtime)

    def add_message(self, address, value):
        """
            Add a very basic message with one value to the bundle.
        """
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add_arg(value)
        self.bndl.add_content(t_message.build())


    def add_voice_on(
            self
            , midi_note=None
            , osc_note=None # 0..1.0
            , velocity=None
            , frequency=None # in Hz
            , is_cut=True
            , is_relative=False
            , is_velocity_relative=True
            , synth_id=1
            , voice_id=1):
        """
        format according to https://github.com/fabb/SynOSCopy/wiki, but using
        float instead of integers, since pd only supports 24bit integer, but
        32bit float.

        panning: 0..0.5..1
        SYN/IDx/Vx/ON, TFTi(Fi):
          T: cut previous voice
          F: absolut value (instead of relative)
          T: value is note number (F=frequency)
          f: actual note number: osc_note * 127
            MIDI [0..127]
            / frequency in Hz: Hz
          T: velocity is absolute
          i: velocity value [0..1.0]
        """
        is_note_number = True
        has_velocity = False
        if synth_id is None or voice_id is None:
            logging.error("No synth or voice defined")
        if osc_note is not None:
            if midi_note is not None:
                logging.warning("OSC and MIDI note specified, dropping MIDI note")
            midi_note = osc_note * 127
        if midi_note is not None:
            if midi_note < 0:
                midi_note = 0
                logging.warning("MIDI note < 0, setting to 0")
            elif midi_note > 127:
                midi_note = 127
                logging.warning("MIDI note > 127, setting to 127")
        if midi_note is not None and frequency is not None:
            logging.warning("Note number and frequency set, dropping frequency")
            frequency = None
        if frequency is not None:
            is_note_number = False
            osc_frequency = frequency
        if velocity is not None:
            has_velocity = True

        address = ("/SYN/ID%s/V%s/ON" % (synth_id, voice_id))

        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add_arg(is_cut)
        t_message.add_arg(is_relative)
        t_message.add_arg(is_note_number)
        if is_note_number:
            t_message.add_arg(midi_note)
        else:
            t_message.add_arg(osc_frequency)
        if has_velocity:
            t_message.add_arg(is_velocity_relative)
            t_message.add_arg(velocity)
        self.bndl.add_content(t_message.build())


    def add_voice_off(
            self
            , velocity=None
            , is_relative=False
            , synth_id=1
            , voice_id=1):
        """
        Add a note off message.
        """
        if velocity:
            has_velocity = True
        address = ("/SYN/ID%s/V%s/OFF" % (synth_id, voice_id))
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        if has_velocity:
            t_message.add(is_relative)
            t_message.add(velocity)
        self.bndl.add_content(t_message.build())


    def add_volume(
            self
            , volume
            , is_relative=False
            , synth_id=1
            , voice_id=1):
        """
        Add a volume message.
        """
        address = ("/SYN/ID%s/V%s/VOL" % (synth_id, voice_id))
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add_arg(is_relative)
        t_message.add_arg(volume)
        self.bndl.add_content(t_message.build())


    def add_pitch(
            self
            , pitch # in Hz or Cent
            , is_relative=False
            , is_hz=False
            , synth_id=1
            , voice_id=1):
        """
        Add a pitch message.
        """
        address = ("/SYN/ID%s/V%s/PITCH" % (synth_id, voice_id))
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add_arg(is_relative)
        t_message.add_arg(is_hz)
        t_message.add_arg(pitch)
        self.bndl.add_content(t_message.build())


    def add_pan(
            self
            , pan_lr
            , pan_bf=None # back - front
            , pan_bt=None # top - bottom
            , is_relative=False
            , synth_id=1
            , voice_id=1):
        """
        Add panning message (in 3D)
        """
        address = ("/SYN/ID%s/V%s/PAN" % (synth_id, voice_id))
        has_bf = False
        has_bt = False
        if pan_bf:
            has_bf = True
            if  pan_bt: # TODO: make more sensible
                has_bt = True
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add_arg(is_relative)
        t_message.add_arg(pan_lr)
        if has_bf:
            t_message.add_arg(pan_bf)
        if has_bt:
            t_message.add_arg(pan_bt)
        self.bndl.add_content(t_message.build())

    def add_param(
            self
            , value
            , is_relative=False
            , param_id=1
            , synth_id=1
            , voice_id=1):
        """
        Add a parameter value to the message.
        """
        address = ("/SYN/ID%s/V%s/P%s" % (synth_id, voice_id, param_id))
        t_message = osc_message_builder.OscMessageBuilder(address=address)
        t_message.add(is_relative)
        t_message.add(value)
        self.bndl.add_content(t_message.build())

    def build(self):
        return self.bndl.build()

    def send(self):
        """
        Build the bundle and send the message.
        """
        self.client.send(self.bndl.build())
