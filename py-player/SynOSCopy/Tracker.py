"""
Plays a track

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""


import logging
import os.path
import math
import time
import pandas as pd
from operator import itemgetter
from SynOSCopy.Bundle import * 

class Tracker(object):

    def __init__(self, track, start_time = 0):
        self.tracker = track
        self.start_time = start_time
        self.song = {}
        self.struct = {}
        self.extract_struct()
        self.build_song()


    def extract_struct(self):
        for name in self.tracker.columns:
            [_, synth_id, voice_id, parameter, dtype] = name.split("-")
            synth_id = int(synth_id)
            voice_id = int(voice_id)
            if synth_id in self.struct.keys():
                if voice_id in self.struct[synth_id].keys():
                    self.struct[synth_id][voice_id][parameter] = dtype
                else:
                    self.struct[synth_id][voice_id] = {parameter:dtype}
            else:
                self.struct[synth_id] = {voice_id: {parameter:dtype}}


    def build_song(self):
        rs = "s-%d-%d-%s-%s"
        for index, row in self.tracker.iterrows():
            # bndl = Bundle('127.0.0.1', 8000)
            bndl = Bundle()
            for sid in self.struct.keys():
                for vid in self.struct[sid].keys():
                    kkeys = self.struct[sid][vid].keys()
                    if 'NOTE' in kkeys:
                        nhead = rs % (sid, vid, 'NOTE', self.struct[sid][vid]['NOTE'])
                        if not math.isnan(row[nhead]):
                            if 'VELOCITY' in kkeys:
                                vhead = rs % (sid, vid, 'VELOCITY', self.struct[sid][vid]['VELOCITY'])
                                if self.struct[sid][vid]['NOTE'] == 'midi':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , midi_note = int(row[nhead])
                                            , velocity = float(row[vhead]))
                                elif self.struct[sid][vid]['NOTE'] == 'freq':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , frequency = float(row[nhead])
                                            , velocity = float(row[vhead]))
                                elif self.struct[sid][vid]['NOTE'] == 'val':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , osc_note = float(row[nhead])
                                            , velocity = float(row[vhead]))
                            else: # VELOCITY
                                if self.struct[sid][vid]['NOTE'] == 'midi':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , midi_note = int(row[nhead]))
                                elif self.struct[sid][vid]['NOTE'] == 'freq':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , frequency = float(row[nhead]))
                                elif self.struct[sid][vid]['NOTE'] == 'val':
                                    bndl.add_voice_on(
                                            synth_id=sid
                                            , voice_id=vid
                                            , osc_note = float(row[nhead]))
                    elif 'VOLUME' in kkeys:
                        vhead = rs % (sid, vid, 'VOLUME', self.struct[sid][vid]['VOLUME'])
                        bndl.add_volume(
                                synth_id=sid
                                , voice_id=vid
                                , volume=float(row[vhead]))
                    elif 'PAN' in kkeys:
                        phead = rs % (sid, vid, 'PAN', self.struct[sid][vid]['PAN'])
                        bndl.add_pan(
                                synth_id=sid
                                , voice_id=vid
                                , pan_lr=float(row[phead]))
            self.song[index] = bndl 

    def play(self):
        starttime = time.time() - (self.start_time / 1000.)
        client = udp_client.UDPClient('127.0.0.1', 8000)
        for note_time in sorted(self.song.keys()):
            if note_time > self.start_time:
                print(note_time)
                next_time = starttime + (note_time / 1000.)
                while time.time() < next_time:
                    time.sleep(0.0001)
                client.send(self.song[note_time].build())

