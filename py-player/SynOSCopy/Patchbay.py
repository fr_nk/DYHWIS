"""
Read a patch bay file and creates pandas DataFrame with exact timing and
information when to play what.

author: Frank Loesche <Frank.Loesche@Plymouth.ac.uk>
"""

import logging
import os.path
import csv
import pandas as pd

class Patchbay(object):

    def __init__(self, filename, datadir):

        self.datadir = datadir
        self.track = None

        with open(filename) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                pf = row['playlist_file']
                cn = row['column_name']
                si = row['SynthID']
                vi = row['VoiceID']
                pa = row['Parameter']

                self.validate(pf, cn, si, vi, pa)

                fp = os.path.join(self.datadir, row['playlist_file'])
                df = pd.read_csv(fp, usecols=['time', row['column_name']],
                        index_col='time')
                df.columns = [self.csv_column_name(cn, si, vi, pa) ]
                if self.track is None:
                    self.track = df
                else:
                    self.track = self.track.join(df, how='outer')


    def csv_column_name(self, colname, synth_id, voice_id, parameter):
        nt = colname.split("_")[-1]
        tp = 'val'
        if not nt in 'val':
            tp = nt
        return("s-%d-%d-%s-%s" % (int(synth_id), int(voice_id), parameter, tp))


    def validate(self, playfile, colname, synth_id, voice_id, parameter):
        fp_playfile = os.path.join(self.datadir, playfile)
        if not os.path.isfile(fp_playfile):
            logging.error("Could not find playlist file {}".format(fp_playfile))
        if not colname.split("_")[-1] in ["freq", "midi", "val"]:
            logging.error('column_name "{}" in {} might be wrong, needs to end '\
                'with _freq, _midi, or _val'.format(colname, fp_playfile))
        intsid = intvid = 0
        try:
            intsid = int(synth_id)
            intvid = int(voice_id)
            if not 0 < intsid < 2**16:
                logging.error('synth_ID is outside the range of 0 to 65536 for '\
                    '"{}" in {}'.format(colname, fp_playfile))
            if not 0 < intvid < 2**16:
                logging.error('voice_ID is outside the range of 0 to 65536 for '\
                    '"{}" in {}'.format(colname, fp_playfile))
        except:
            logging.error('either synth_ID or voice_ID is not a number for '\
                    '\"{}" in {}.'.format(colname, fp_playfile))
        if not parameter in ["NOTE", "VOLUME", "VELOCITY", "PAN"]:
            try:
                intpid = int(parameter)
                if not 0 < intpid < 2**16:
                    logging.error('parameterID is outside the range of 0 to '\
                    '65536 for "{}" in {}'.format(colname, fp_playfile))
            except:
                logging.error('parameter must be one of NOTE, VOLUME, '\
                    'VELOCITY, PAN, or parameter number for "{}" in {}'.format(
                    colname, fp_playfile))

        ## TODO: check for _freq and _midi to Note-mapping



