@ECHO OFF

ECHO "You should be connected to the WiFi 'IMPROV' and all 6 sensor devices should be turned on"

REM https://stackoverflow.com/a/11037921
REM generate a readable time format
FOR /f "tokens=2-8 delims=.:/ " %%a IN ("%date% %time%) DO SET timestamp=%%c-%%a-%%bT%%d-%%e-%%f.%%g

REM make data dir
IF NOT EXIST "data" mkdir "data"

REM start socket-players in parallel
REM CALL instead of START?
START python.exe socket-player.py --sensorip 192.168.0.151 --synthid 1 --silent --outfile ".\data\%timestamp%-01.csv"
START python.exe socket-player.py --sensorip 192.168.0.152 --synthid 2 --silent --outfile ".\data\%timestamp%-02.csv"
START python.exe socket-player.py --sensorip 192.168.0.153 --synthid 3 --silent --outfile ".\data\%timestamp%-03.csv"
START python.exe socket-player.py --sensorip 192.168.0.154 --synthid 4 --silent --outfile ".\data\%timestamp%-04.csv"
START python.exe socket-player.py --sensorip 192.168.0.155 --synthid 5 --silent --outfile ".\data\%timestamp%-05.csv"
START python.exe socket-player.py --sensorip 192.168.0.156 --synthid 6 --silent --outfile ".\data\%timestamp%-06.csv"
