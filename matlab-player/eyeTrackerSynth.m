function eyeTrackerSynth(fname,doDisp)
% eyeTrackerSynth(fname)
%
% Display eye tracker data (for now we ignore pupil dilation)
% Find start of each fixation using velocity  eye -> note onset
% Shape sounds: 
%   velocity peak (pre fixation) -> amplitude
%   sum(LY,RY) -> pitch (0-127)
%   sum(LX,YX) -> envelope rise/fall
% 
% Inputs
%   fname   name of file where csv data is stored, assume it is in the form
%           time,Left eye X (LX),LY,RX,RY
%   doDisp  flag set to display data, and note positions
%
% SD Aug 2016
%
%..........................................................................

% Initialise
if nargin<2, doDisp = 0; end
thresh = 0.1;

% Get data from csv file
x = csvread(fname);
t = x(:,1);
x(:,1) = [];
[n,m] = size(x);

% Temp fix: scale all data (except time) to range 0:1, based on range
% For real time demos we need to know the range the eye tracker can put
% out
x = scaleData(x,n);

% Display raw data
if doDisp
    fig
    subplot(211),plot(t,x(:,[1 2])),title('Left eye gaze'),grid
    subplot(212),plot(t,x(:,[3 4])),title('Right eye gaze'),grid
    xlabel('Elapsed time (ms)')
end

% Get velocity
v = getVelocity(t,x,n);

% Get notes: duration, pitch, amplitude, env rise/fall sampleIndex
notes = getNotes(v,t,x,n,thresh);

% Display temporal data and scan path with note positions
if doDisp
    fig
    subplot(211),plot(t,x,':','linew',2),grid
    hold on
    tNotes = cumsum(notes(:,1));
    for i = 1:size(notes,1)
        stem(tNotes(i),notes(i,2)/127,'filled','markers',notes(i,3)*15,'color',[0 0 0])
    end
    title('Eye gaze and notes'),xlabel('Elapsed time (ms)')
    subplot(212)
    plot(x(:,1),x(:,2),'b',x(:,3),x(:,4),'r','linew',2),grid
    hold on
    for i = 1:size(notes,1)
        ind = notes(i,5);
        plot(mean(x(ind,[1 3])),mean(x(ind,[2 4])),'k.','markers',notes(i,3)*50)
        plot(mean(x(ind,[1 3])),mean(x(ind,[2 4])),'k+','markers',notes(i,3)*30)
    end
end

% Send data to pd
send2PD(notes)


%..........................................................................
%..........................................................................
function y = scaleData(x,n)
mx = max(x);
mn = min(x);
d = repmat((mx-mn),[n 1]);
y = (x - repmat(mn,[n 1]))./d;


%..........................................................................
%..........................................................................
function v = getVelocity(t,x,n)
v = zeros(n,1);
for i = 2:n
    d = abs((x(i,:)-x(i-1,:)))/t(i);
    v(i) = sum(d);
end
v(1) = v(2);
v = scaleData(v,n);


%..........................................................................
%..........................................................................
function notes = getNotes(v,t,x,n,thresh)
% Each row of notes contains the following: duration, pitch, amplitude, env 
% rise/fall sampleIndex. env is a number range 0:1 which indicates the
% proprtion of total duration taken by the rise time, decay is 1-rise

notes = [];
doRiseFall = 1; % rise==1, fall == 2
t0 = 1;
for i = 2:n
    switch doRiseFall
        case 1 % rise
            if (v(i-1) > thresh) & (v(i) < v(i-1))
                doRiseFall = 3 - doRiseFall;
            end
        case 2 % fall
            if v(i)<thresh
                doRiseFall = 3 - doRiseFall;
                % time pitch amp env
                event = [t(i-1) sum(x(i-1,[2 4])) max(v(t0:i)) sum(x(i-1,[1 3])) i-1];
                notes = [notes; event];
                t0 = i;
            end
    end
end

% Convert elapsed times to durations
nn = size(notes,1);
notes(2:nn,1) = notes(2:nn,1) - notes(1:nn-1,1);
% notes(1,1) = 0;
% Scale note spec ### temp fix
% pitch 0:127 - in practice 40:100
notes(:,2) = round(scaleData(notes(:,2),nn)*60 + 40);
% amplitude, riseFall - 0.1:0.9
notes(:,[3 4]) = 0.8*scaleData(notes(:,[3 4]),nn)+0.1;

%..........................................................................
%..........................................................................
function send2PD(notes)

% Comment: Instead of directly opening an UDP port, you just need to
% initialise a "Bundle", either just with the port for a local puredata or
% with address and port for a remote machine.
%
% Local: b = Bundle(8000);
% Remote: b= Bundle('192.168.0.5', 8000);
%
%u = udp('127.0.0.1',8000);
%fopen(u);
b = Bundle('192.168.0.28', 8000);
clc
disp('Event  Duration(s) MidiPitch')
for i = 1:size(notes,1)
    disp([i notes(i,1)/1000 notes(i,2)]),drawnow
    %oscsend(u,'/ch1','i', notes(i,2)); % pitch midi 0:127 (50:100)
    %oscsend(u,'/ch2','f', notes(i,3)); % amplitude 0.1:0.9
    %oscsend(u,'/ch3','f', notes(i,4)); % envelope rise proportion 0.1:0.9
    % Comment: Each synthesizer can have any number of "voices". The
    % following code plays a single note [notes( i, 2)] on Synthesizer 1, 
    % Voice 1 and plays it with a velocity of [notes(i, 4)]. This velocity
    % is typically used to controll attack and decay times of a sound
    % (think of speed you press a key on a piano). It also adds the volume
    % of the voice to the same bundle [notes(i, 3)].     
    b.voice_on_midi(1, 1, notes(i,2), notes(i,4));
    b.volume(1, 1, notes(i, 3));
    % both of the signals are sent at the same time in the same "bundle".
    b.send();
    pause(notes(i,1)/1000) % wait for elapsed time
end
% sound off
%oscsend(u,'/ch1','i', 0); % pitch midi 0:127 (50:100)
%oscsend(u,'/ch2','f', 0);
% Comment: Synthesizer 1, Voice 1 is sent the "note off" signal.
b.voice_off(1, 1);
b.send();

% oscsend(u,'/ch1','ifsINBTF', 1, 3.14, 'hello',[],[],false,[],[]); 
% fclose(u);