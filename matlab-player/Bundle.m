classdef Bundle < handle
    %   BUNDLE construct and send an OSC bundle to an udp address. This is
    %   created for the ColLaboratoire Summer School project
    %   "Remapping the Sensorium: Do You Hear What I See?"
    %   https://bitbucket.org/account/user/ColLaboratoire/projects/DYHWIS
    % 
    %   The OSC message construction part is Copyright (c) 2011, Mark
    %   Marijnissen (copyright notice see below).
    %
    %   The OSC message protocol is adapted from 
    %   https://github.com/fabb/SynOSCopy/wiki.
    %
    %   (c) 2016, Frank Loesche <Frank.Loesche@Plymouth.ac.uk>


    properties%(Access = private)
        udp
        BundleData
        littleEndian
    end
    
    methods
        
        function obj = Bundle(varargin)
            % BUNDLE Construct a bundle with and UDP object
            %   Bundle(udp) is a OSC Bundle that can be sent to the udp
            %   address using send(). See also SEND
            if nargin == 1
                obj.udp = udp('127.0.0.1', varargin{1});
            elseif nargin == 2
                obj.udp = udp(varargin{1}, varargin{2});
            else
                error('must be initialised with port (for localhost) or address and port');
            end
            
            fopen(obj.udp);
            
            %figure out little endian for int/float conversion
            [~, ~, endian] = computer;
            obj.littleEndian = endian == 'L';
            
            obj.initialise();
        end

        function voice_on_midi(obj, synth_id, voice_id, midi_note, velocity)
            % VOICE_ON_MIDI adds a MIDI NOTE ON message to the bundle
            %
            %   BUNDLE.VOICE_ON_MIDI(synth_id, voice_id, midi_note) adds
            %   a message containing the midi_note (between 0 and 127) to
            %   the bundle. Synth_id and voice_id should be integer numbers
            %   to specify the synthesizer and the voice within that
            %   synthesizer.
            %
            %   BUNDLE.VOICE_ON_MIDI(synth_id, voice_id, midi_note,
            %   velocity) sends an additional velocity value (between 0.0
            %   and 1.0) in the message.
            address = sprintf('/SYN/ID%d/V%d/ON', synth_id, voice_id);
            midi_note = obj.fence(midi_note, 0, 127);
            if nargin == 5
                velocity = obj.fence(velocity, 0.0, 1.0);
                obj.add_message(address, 'TFTfTf', [], [], [], midi_note, [], velocity);
            elseif nargin == 4
                obj.add_message(address, 'TFTf', [], [], [], midi_note);
            else
                warning('wrong number of arguments');
            end
        end
        
        function voice_on_freq(obj, synth_id, voice_id, freq, velocity)
            % VOICE_ON_FREQ adds a frequency NOTE ON message to the bundle
            %
            %   BUNDLE.VOICE_ON_FREQ(synth_id, voice_id, freq) adds
            %   a message containing the frequency(between 20 and 25000Hz) 
            %   to the bundle. Synth_id and voice_id should be integer 
            %   numbers to specify the synthesizer and the voice within 
            %   that synthesizer.
            %
            %   BUNDLE.VOICE_ON_FREQ(synth_id, voice_id, freq,
            %   velocity) sends an additional velocity value (between 0.0
            %   and 1.0) in the message.
            address = sprintf('/SYN/ID%d/V%d/ON', synth_id, voice_id);
            freq = obj.fence(freq, 20, 25000);
            if nargin == 5
                velocity = obj.fence(velocity, 0.0, 1.0);
                obj.add_message(address, 'TFFfTf', [], [], [], freq, [], velocity);
            elseif nargin == 4
                obj.add_message(address, 'TFFf', [], [], [], freq);
            else
                warning('wrong number of arguments');
            end
        end
        
        function voice_on_oscnote(obj, synth_id, voice_id, osc_note, velocity)
            % VOICE_ON_OSCNOTE adds a float NOTE ON message to the bundle
            %
            %   BUNDLE.VOICE_ON_OSCNOTE(synth_id, voice_id, osc_note) adds
            %   a message containing a note number between 0.0 and 1.0 
            %   to the bundle. They will be translated to MIDI notes 0 to 127.
            %   Synth_id and voice_id should be integer numbers to specify 
            %   the synthesizer and the voice within that synthesizer.
            %
            %   BUNDLE.VOICE_ON_OSCNOTE(synth_id, voice_id, osc_note,
            %   velocity) sends an additional velocity value (between 0.0
            %   and 1.0) in the message.
            midi_note = round(osc_note * 127);  % range check will be done in voice_on_midi
            obj.voice_on_midi(synth_id, voice_id, midi_note, velocity);
        end
            
        function voice_off(obj, synth_id, voice_id, velocity)
            % VOICE_OFF adds a NOTE OFF message to the bundle
            %
            %   BUNDLE.VOICE_OFF(synth_id, voice_id) adds a NOTE OFF
            %   message for voice number VOICE_ID on synthesizer SYNTH_ID
            %   to the BUNDLE.
            address = sprintf('/SYN/ID%d/V%d/OFF', synth_id, voice_id);
            if nargin == 4
                velocity = obj.fence(velocity, 0.0, 1.0);
                obj.add_message(address, 'Ff', [], velocity);
            elseif nargin == 3
                obj.add_message(address);
            else
                warning('wrong number of arguments')
            end
        end
        
        function volume(obj, synth_id, voice_id, volume)
            % VOLUME adds VOLUME message to bundle
            %
            %   BUNDLE.VOLUME(synth_id, voice_id, volume) adds a VOLUME
            %   message for voice number VOICE_ID on synthesizer SYNTH_ID
            %   to the BUNDLE. Volume should be between 0.0 and 1.0.
            address = sprintf('/SYN/ID%d/V%d/VOL', synth_id, voice_id);
            volume = obj.fence(volume, 0.0, 1.0);
            obj.add_message(address, 'Ff', [], volume);
        end
        
        function pitch(obj, synth_id, voice_id, pitch)
            % PITCH adds PITCH message to bundle
            %
            %   BUNDLE.PITCH(synth_id, voice_id, pitch) adds a pitch in
            %   cents to the message. The message is addressed to
            %   voice VOICE_ID on synthesizer SYNTH_ID.
            address = sprintf('/SYN/ID%d/V%d/PITCH', synth_id, voice_id);
            obj.add_message(address, 'FFf', [], pitch);
        end
        
        function pan(obj, synth_id, voice_id, pan_lr, pan_bf, pan_bt)
            % PAN adds panning message to bundle
            %
            %   BUNDLE.PAN(synth_id, voice_id, pan_lr) adds a stereo
            %   panning message to the bundle. Left is 0.0, center is 0.5
            %   and right is 1.0.
            %
            %   BUNDLE.PAN(synth_id, voice_id, pan_lr, pan_bf) adds a 2D
            %   panning message to the bundle. In addition to left-right,
            %   pan_bf represents back (=0.0), center (=0.5), and front
            %   (=1.0).
            %
            %   BUNDLE.PAN(synth_id, voice_id, pan_lr, pan_bf, pan_bt) adds
            %   a 3D panning message to the bundle. In addition to
            %   left-right and back-front, it includes information for
            %   bottom (=0.0), center (=0.5), and top (=1.0) on the
            %   vertical axis.
            address = sprintf('/SYN/ID%d/V%d/PAN', synth_id, voice_id);
            pan_lr = obj.fence(pan_lr, 0.0, 1.0);
            if nargin == 4
                obj.add_message(address, 'Ff', [], pan_lr);
            elseif nargin == 5
                pan_bf = obj.fence(pan_bf, 0.0, 1.0);
                obj.add_message(address, 'Fff', [], pan_lr, pan_bf);
            elseif nargin == 6
                pan_bt = obj.fence(pan_bt, 0.0, 1.0);
                obj.add_message(address, 'Ffff', [], pan_lr, pan_bf, pan_bt);
            end
        end
        
        function param(obj, synth_id, voice_id, param_id, value)
            % PARAM adds a parameter message to the bundle
            %
            %   BUNDLE.PARAM(synth_id, voice_id, param_id, value) adds a
            %   numeric value for a numbered parameter to the bundle.
            %   PARAM_ID should be an integer number > 0 representing a
            %   parameter, value can be float.
            address = sprintf('/SYN/ID%d/V%d/P%d', synth_id, voice_id, param_id);
            obj.add_message(address, 'Ff', [], value);
        end
        
        function send(obj)
            % SEND send and empty the bundle
            %   This sends out the bundle, which consists of a number of
            %   messages, to the address specified in udp during the
            %   construction. The bundle will be empty afterwards and can
            %   be reused.
            %
            %   See also BUNDLE
            
            fwrite(obj.udp, obj.BundleData);            
            obj.initialise();
        end
    end
    
    
    methods(Access = private)
        
        
        function delete(obj)            
            fclose(obj.udp);
        end
        
        %Conversion from double to float
        function float = oscfloat(obj, float, littleEndian)
            if littleEndian
                float = typecast(swapbytes(single(float)),'uint8');
            else
                float = typecast(single(float),'uint8');
            end;
        end
        
        %Conversion to int
        function int = oscint(obj, int,littleEndian)
            if littleEndian
                int = typecast(swapbytes(int32(int)),'uint8');
            else
                int = typecast(int32(int),'uint8');
            end;
        end
              
        %Conversion to string (null-terminated, in multiples of 4 bytes)
        function string = oscstr(obj, string)
            string = [string 0 0 0 0];
            string = string(1:end-mod(length(string),4));
        end
        
        function ret = fence(obj, val, min, max)
            ret = val;
            if val < min
                ret = min;
            elseif val > max
                ret = max;
            end;
        end
        
        function initialise(obj)
            obj.BundleData = [obj.oscstr('#bundle') obj.oscint(0, true) obj.oscint(1, true)];
        end
        
        
        function add_message(obj, path, varargin)
            % This is copied from https://uk.mathworks.com/matlabcentral/fileexchange/31400-send-open-sound-control--osc--messages
            %
            
            %   Copyright (c) 2011, Mark Marijnissen
            %   All rights reserved.
            
            %   Redistribution and use in source and binary forms, with or without
            %   modification, are permitted provided that the following conditions are
            %   met:
            %
            %       * Redistributions of source code must retain the above copyright
            %         notice, this list of conditions and the following disclaimer.
            %       * Redistributions in binary form must reproduce the above copyright
            %         notice, this list of conditions and the following disclaimer in
            %         the documentation and/or other materials provided with the distribution
            %
            %   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
            %   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
            %   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
            %   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
            %   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
            %   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
            %   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
            %   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
            %   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
            %   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
            %   POSSIBILITY OF SUCH DAMAGE.
            
            %
            % Builds an Open Sound Control (OSC) message
            %
            % path = path-string
            % types = string with types of arguments,
            %    supported:
            %       i = integer
            %       f = float
            %       s = string
            %       N = Null (ignores corresponding argument)
            %       I = Impulse (ignores corresponding argument)
            %       T = True (ignores corresponding argument)
            %       F = False (ignores corresponding argument)
            %       B = boolean (not official: converts argument to T/F in the type)
            %    not supported:
            %       b = blob
            %
            % args = arguments as specified by types.
            %
            % EXAMPLE
            %       u = udp('127.0.0.1',7488);
            %       fopen(u);
            %       oscsend(u,'/test','ifsINBTF', 1, 3.14, 'hello',[],[],false,[],[]);
            %       fclose(u);
            %
            % See http://opensoundcontrol.org/ for more information about OSC.
            
            % MARK MARIJNISSEN 10 may 2011 (markmarijnissen@gmail.com)
            
            % set type
            if nargin > 2,
                types = obj.oscstr([',' varargin{1}]);
            else
                types = obj.oscstr(',');
            end;
            
            % set args (either a matrix, or varargin)
            if nargin == 4 && length(types) > 2
                args = varargin{2};
            elseif length(types) > 2
                args = varargin(2:end);
            else
                args = [];
            end;
            
            % convert arguments to the right bytes
            data = [];
            for i=1:length(args)
                switch(types(i+1))
                    case 'i'
                        data = [data obj.oscint(args{i}, obj.littleEndian)];
                    case 'f'
                        data = [data obj.oscfloat(args{i},obj.littleEndian)];
                    case 's'
                        data = [data obj.oscstr(args{i})];
                    case 'B'
                        if args{i}
                            types(i+1) = 'T';
                        else
                            types(i+1) = 'F';
                        end;
                    case {'N','I','T','F'}
                        %ignore data
                    otherwise
                        warning(['Unsupported type: ' types(i+1)]);
                end;
            end;
            
            data = [obj.oscstr(path) types data];
            tdat = obj.BundleData;
            datlen = obj.oscint(length(data), obj.littleEndian);
            obj.BundleData  = [tdat, datlen, data];
        end
        
    end
    
end

