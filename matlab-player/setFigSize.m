function fPos = setFigSize
% fPos = setFigSize
%
% Sets figure size to occupy the righthand half of the screen
%
% Output
%   fPos    figure position [left, bottom, width, height]
%
% SD SCANDLE Nov 2010
%
%..........................................................................

set(0,'Units','pixels')
s = get(0,'ScreenSize');
h = 0.9*s(4);
w = s(3)/2;
fPos = [s(1)+w, s(2)+0.05*h, 0.99*w, 0.95*h];

