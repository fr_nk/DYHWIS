# Sound production

This repository is for the project ["Remapping the sensorium: Do You Hear What I See?"](https://collaboratoire.cognovo.eu/projects#p2). This project is part of the [CogNovo](https://CogNovo.eu) summer school ["ColLaboratoire"](https://ColLaboratoire.CogNovo.eu).


## What is it for?

The repository is intended to store everything related to sound production. I mainly focus on puredata for that. Any data that is going to be sonified is received via OSC, using a modified version of the SynOSCopy protocol (see description below).


## How to set up?

To use those patches, you need a working version of [puredata (pd)](https://puredata.info/). I am currently working with the [pd-l2ork](http://l2ork.music.vt.edu/main/), but a vanilla pd with a [mrpeach library](https://puredata.info/downloads/mrpeach) should work as well.

A good place to start is the file `sample-setup.pd` which has a very basic patch receiving OSC messages. They are then sent through the `osc2midi.pd` patch which creates four outputs (frequency, velocity, volume, and pan). These outputs are then synthesized. Have a look at the very basic sample `synth-template.pd` patch to get an idea how to implement your own synthesizer.


## Modified SynOSCopy protocol

To avoid running into the same problems others have already solved, I tried to follow an existing protocol based on OSC. The one described as [SynOSCopy](https://github.com/fabb/SynOSCopy/wiki) seemed quite promising. It defines a REST "API" to modify single voices and whole synthesizers. SynOSCopy is based on int32 values. After some test runs it turned out, that pd only supports float, which means their integer values become unreliable above 24bit. Also their center values are not really *intuitive* (for example a maximum volume would be 2147483648, a center panning would be 1073741823). Since puredata supports 32bit float values, I decided to use the same principles, but send the values in float between 0 and 1 instead. This also allows to use untranslated frequencies, cents, and MIDI notes. Overall the values seem more intuitive with maximum volume being 1.0, MIDI notes from 0.0 to 127.0 etc. See a full description below. Note, that this is basically a 1:1 copy from the original [SynOSCopy](Protocol.md) protocol, with some minor changes in regards to the data format.




## Problems / Questions?

If you have any problems with the setup, get in contact with [Frank](https://CogNovo.eu/frank-loesche).