function initialize(box)
	fn = box:get_setting(2)
	box:log("Info", "Trying to write to file " .. fn)
	fl,err = io.open(fn, "w")
	if err then
		box:log("Error", err)
	end
	fl:write("SystemTime,StreamTime\n")
end

function uninitialize(box)
	fl:close()
end

function process(box)
	while true do
		local ct = box:get_current_time()
		fl:write(os.date() .. "," .. ct .."\n")
		while box:get_current_time() < ct + 0.01 do
			box:sleep() 
		end
	end
	
end
