# Data Recording

This repository is for the project ["Remapping the sensorium: Do You Hear What I See?"](https://collaboratoire.cognovo.eu/projects#p2). This project is part of the [CogNovo](https://CogNovo.eu) summer school ["ColLaboratoire"](https://ColLaboratoire.CogNovo.eu).

It includes the vide player itself which records time stamps for the played movie, and openVIBE based tools to record the EEG signals from a EMOTIV EPOC+ device.

# Video player

Everything related to this setup is in the path `video-player`.

## What is it for?

The video player plays a video and records the UTC timestamp for starting the video as well as the end. It also records if the video gets out of sync. This will allow to identify what signals were played to the participants at any time.

## How to set up?

PsychoPy is required. For Windows and Mac the [Standalone PsychoPy](https://github.com/psychopy/psychopy/releases) is suggested (and tested). On Linux just use your package manager to install the latest version.

## What is it?

Just run the script `ss_vid_player.py` and you should be seeing the video. Check the `logged_data` path afterwards for the logged time stamps.

## Problems / Questions?

If you have any problems with the video-player, get in contact with [David](https://CogNovo.eu/david-bridges).


# EEG Recorder

Everything related to this setup is in the path `openVibe`.

## What is it for?

The project includes the recording of EEG data as well as physiological measures.

EEG data is recorded with two Emotiv EPOC+ devices using [openVIBE](http://openvibe.inria.fr/). This repository holds the setup files and scripts necessary to create the required output.


## How to set up?

Install openVIBE for your platform. It is currently tested with openVIBE-1.2 on Windows7. Open the scenario (XML-file) with the openVIBE designer. Make sure that the lua-script is in the same directory as the scenario file and a subdirectory `data` exists. Check the configuration of the Acquisation Plugin to point to the right server. Once you start the scenario, it should start creating four output files in `data`.

## What is it?

Since we only need the raw EEG sensor data, I considered using [lab streaming layer (LSL)](https://github.com/sccn/labstreaminglayer) to record the streams from the Emotiv EPOC+ EEG devices. The time signal set as `created_at` in the XDF file wasn't meaningful and could not be translated into a local time. But the local time is necessary to synchronise the data stream with other data recorded. And I couldn't get the scripting feature in the LabRecorder to work. Therefore I settled with using the openVIBE designer basically as a recording software.


### DYHWIS-Recorder.xml

This file is an [openVibe](http://openvibe.inria.fr) designer scenario. It reads data from an acquisition server and writes the stream to three different data files: a GDF, an EDF, and an CSV file. I requires the `timesaver.lua` script to be referenced (which automatically happens if both files are in the same directory).

The GDF file is a [General Data Format for BiomedicalSignals](https://en.wikipedia.org/wiki/General_Data_Format_for_Biomedical_Signals). Using the BioSig toolbox, it can be read into [MATLAB](https://uk.mathworks.com/products/matlab/) and its toolbox [EEGLAB](https://sccn.ucsd.edu/eeglab/). The generated EDF file is a [European Data Format](https://en.wikipedia.org/wiki/European_Data_Format) and can be used in a similar way as the GDF file. Both of those file types store some additional information besides the pure data stream, such as events, times, and subject information, but require special libraries and software to access the data.

The CSV file ([Comma-separated values](https://en.wikipedia.org/wiki/Comma-separated_values)) is plain text and stores each of the streams as a column plus one additional column with the time since the recording started.


### timesaver.lua

While the time received from the acquisition server was just as meaningless as for the LSL layer, the scripting boxes for openVIBE seemed to provide a solution. I first tried the python box, but this resulted in a very high load on the machine and data being dropped due to the high load. A Lua box, on the other side, would run without requiring too many system resources. Nevertheless Lua only provides time with seconds precision. Therefore the `timesaver.lua` script saves the experiment time (in (sub?)-millisecond precision) and the local time (with second precision) several times a second. This ultimately allows a mapping between experiment time and the switch between seconds on the local system and therefore, in a second step, a synchronisation with other data.


## Problems / Questions?

If you have any problems with the setup, get in contact with [Frank](https://CogNovo.eu/frank-loesche).
