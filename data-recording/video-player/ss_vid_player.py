
from psychopy import visual, core, event,monitors,logging
import ppc, csv, time, datetime # has writer class
import numpy

# Create psychopy window

my_monitor = monitors.Monitor('testMonitor') # Create monitor object from the variables above. This is needed to control size of stimuli in degrees.
win = visual.Window(monitor=my_monitor, color=[-1,-1,-1],units='deg', size=[1366, 768], fullscr=True, allowGUI=False)  # Initiate psychopy Window as the object "win", using the myMon object from last line. Use degree as units!

# Where to write logged data

SAVE_FOLDER = 'logged_data'  # Log is saved to this folder. The folder is created if it does not exist.
writer = ppc.csvWriter(saveFolder=SAVE_FOLDER)

# Movie details

mov = visual.MovieStim2(win, 'DYHWIS_v4.mov',
    flipVert=False, flipHoriz=False, loop=False, volume=1.0)

# Set lists for timestamps - every 30 seconds?

increment=1 # How often to log times in seconds
time_index=numpy.arange(0,mov.duration,increment)
list_index = range(0,len(time_index))
i=0 # index for times
clock = core.Clock()
# Create list of dicts for logging times

time_dict=[]
for nums in list_index:
    time_dict+=[{
    'index': nums,
    'movie_time_index': time_index[nums], # Times to match to film time
    'movie_time_actual': '',              # Actual film times to be logged
    'system_time': '', # UTC
    'UTC_time':'',
    'msec_from_zero_UTC':'',
    'trial_time': '',
    }]

# Get UTC time to microsecond accuracy

def TimestampMillisec64():
    return int((datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000) 
    
# Time in ms function

def ms_timing(current, start):
    time=current-start
    return time
    
def set_dict(movie, system, UTC, MS,TT):
    time_dict[i]['movie_time_actual']=movie
    time_dict[i]['system_time']=system
    time_dict[i]['UTC_time']=UTC
    time_dict[i]['msec_from_zero_UTC']=MS
    time_dict[i]['trial_time']=TT
    return writer.write(time_dict[i])

# Begin film, play until finish

while mov.status != visual.FINISHED:
    
    movie_time=mov.getCurrentFrameTime()
        
    # Get start time of film in UTC
    if mov.getCurrentFrameTime()  is None:
        # Set trial start to zero
        win.callOnFlip(clock.reset)
        win.flip()
        trial_start=clock.getTime()
        start_time=TimestampMillisec64()
        #Set movie start time to zero
        movie_time=0
        # Write first trial
        set_dict(movie_time,time.strftime('%H-%M-%S'),TimestampMillisec64(), ms_timing(TimestampMillisec64(),start_time), trial_start)
        i=i+1
    
    mov.draw() # draw movie
    win.flip() # present movie 
    
    #Get movie times
    
    if movie_time==time_index[i]:
        set_dict(movie_time,time.strftime('%H-%M-%S'),TimestampMillisec64(), ms_timing(TimestampMillisec64(),start_time), clock.getTime())
        i=i+1  
    # Quit keys        
    for key in event.getKeys():
        if key in ['escape', 'q']:
            set_dict(movie_time,time.strftime('%H-%M-%S'),TimestampMillisec64(), ms_timing(TimestampMillisec64(),start_time),clock.getTime())
            i=i+1
            win.close()
            core.quit()


set_dict(movie_time,time.strftime('%H-%M-%S'),TimestampMillisec64(), ms_timing(TimestampMillisec64(),start_time), clock.getTime())
  
writer.write(time_dict[i])

win.close()
core.quit()